package ChatServer;

import Utility.ChatProtocol;
import Utility.Message;
import Utility.Message.MessageIdentifier;
import Utility.MessageContent;

/**
 * ChatServerProtocol is used by the server to handle different types of
 * messages it could receive. Depending on the message identifier,
 * it will process that identifier accordingly by using utility functions
 * given by the server.
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class ChatServerProtocol implements ChatProtocol {

    private ChatServer chatServer;

    /**
     * Instantiates ChatServerProtocol.
     * @param chatServer Server that the protocol will be handling messages for.
     */
    public ChatServerProtocol(ChatServer chatServer){
        this.chatServer = chatServer;
    }

    /**
     * Used to handle the different types of messages it can receive, depending on
     * the different type of messages it will be using the chatServer's utility
     * functions to properly process. For example, if a CHAT_MSG is passed, then
     * the protocol will update the server with the message and send a reply. Ensures
     * that replies are sent to the user of this method.
     * @param message received message that will be read
     */
    @Override
    public Message handleMessage(Message message) {

        MessageContent msg = message.getMessageContent();
        Message reply = null;

        switch (message.getMessageIdentifier()){
            case CONNECTION_REQ:
                synchronized (this){
                    if(chatServer.isUserActive(msg.getSourceUser())){
                        reply = new Message(MessageIdentifier.USERNAME_IN_USE, null);
                    }else{
                        chatServer.createNewUser(msg.getSourceUser());
                        reply = new Message(MessageIdentifier.CONNECTION_ACCEPT, null);
                    }
                }
                break;
            case CHAT_MSG:
                String destinationUser = msg.getDestinationUser();

                if(chatServer.isUserActive(destinationUser)){
                    reply = new Message(MessageIdentifier.OKAY, null);
                }else{
                    reply = new Message(MessageIdentifier.MSG_PENDING, null);
                }

                if(!chatServer.doesUserMessageQueueExist(destinationUser)){
                    chatServer.createUserMessageQueue(destinationUser);
                }

                chatServer.storeMessage(destinationUser, msg);
                break;
            case GET_NEXT_QUEUED_MSG:
                reply = new Message(MessageIdentifier.OKAY,
                        chatServer.getNextQueuedMessageForUser(msg.getSourceUser()));
                break;
            case TIMESTAMP_DATA:
                reply = new Message(MessageIdentifier.OKAY, null);
                chatServer.calculateAndStoreTimestampData(msg);
                break;
            case DISCONNECT:
                chatServer.deleteUser(msg.getSourceUser());
                reply = new Message(MessageIdentifier.OKAY, null);
                break;
        }

        return reply;
    }
}
