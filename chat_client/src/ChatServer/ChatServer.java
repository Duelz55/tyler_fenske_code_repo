package ChatServer;

import Utility.MessageContent;
import Utility.NotificationServer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.BindException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This ChatServer holds data for chat users, this data includes a list
 * of active users that are online, and the stored messages for offline users.
 * When the server launches it makes use of a NotificationServer to accept messages
 * from chat users. This class provides utility functions for protocols to use, which
 * allows for the proper handling of messages.
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class ChatServer {

    private List<String> activeUsers;
    private Map<String, Queue<MessageContent>> userPendingMessages;
    private boolean analysisFileIsCreated = false;

    /**
     * Initializes data structures and creates a NotificationServer to accept messages
     * from chat users.
     * @param portNumber port number that the server will use to launch.
     */
    public ChatServer(int portNumber){

        ChatServerProtocol chatServerProtocol = new ChatServerProtocol(this);
        activeUsers = new ArrayList<>();
        userPendingMessages = new HashMap<>();

        try{
            NotificationServer ns = new NotificationServer(portNumber, chatServerProtocol);
            Thread thread = new Thread(ns);
            thread.start();
        }catch(BindException e){
            System.out.println("Port already in use. Terminating session. Please relaunch");
        }catch(IOException e){
            System.out.println("Something went wrong your connection. Please relaunch");
        }
    }

    /**
     * Will create a new user by adding the user to the activeUsers list and create a new users
     * chat queue, whcih contains the users pending messages.
     * @param username name for the new user.
     */
    public void createNewUser(String username){
        activeUsers.add(username);
        createUserMessageQueue(username);
    }

    /**
     * Creates a users chat queue, which contains the users pending message.
     * @param username name for the new user.
     */
    public void createUserMessageQueue(String username){
        if(!doesUserMessageQueueExist(username)){
            userPendingMessages.put(username, new LinkedList<MessageContent>());
        }
    }

    /**
     * Will determine whether the given username has a chat queue.
     * @param username
     */
    public boolean doesUserMessageQueueExist(String username){
        return userPendingMessages.containsKey(username);
    }

    /**
     * Stores a pending message into the users chat queue.
     * @param username name of the user.
     * @param messageContent data that will entered into the users data queue.
     */
    public void storeMessage(String username, MessageContent messageContent){
        userPendingMessages.get(username).add(messageContent);
    }

    /**
     * Will determine whether the user is active in the activeUsers list.
     * @param username name of the user.
     */
    public boolean isUserActive(String username){
        return activeUsers.contains(username);
    }

    /**
     * Will give the messageContent of the head of the users chat message queue.
     * @param username name of the user.
     */
    public MessageContent getNextQueuedMessageForUser(String username){
        Queue<MessageContent> usersQueue = userPendingMessages.get(username);

        //If server disconnects, and clients are still active, the client updater will attempt to get messages
        //from the userPendingMessages queue, and will wont be on the activeUser list, even though they are active.
        //Therefore, if the queue is uninitialized for a user while this method is being called, new user information
        //needs to be created.
        if(usersQueue == null){
            createNewUser(username);
            usersQueue = userPendingMessages.get(username);
        }

        if(usersQueue.peek() != null){
            return usersQueue.remove();
        }
        return null;
    }

    /**
     * Calculates the delivery time given the data contained in the messageContent.
     * Assumes that the delivery time received / sent is contained in the given
     * messageContent, it will then store the data by writing to a file.
     * @param messageContent contains received / sent timestamps.
     */
    public void calculateAndStoreTimestampData(MessageContent messageContent){
        long deliveryTime = calculateDeliveryTime(messageContent);
        storeTimestampData(deliveryTime);
    }

    /**
     * Deletes user from the active users list.
     * @param username name of the user.
     */
    public void deleteUser(String username){
        activeUsers.remove(username);
    }

    /**
     * Given a calculated delivery time will write the data to a file called
     * DeliveryTimes.txt, will overwrite file if server has never written to file.
     * Otherwise, it will append data to the opened file.
     * @param deliveryTime calculated delivery time of receiver and sender.
     */
    private void storeTimestampData(long deliveryTime){
        try{
            String data = deliveryTime + "\n";
            if(!analysisFileIsCreated){
                createAnalysisFile();
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter("AnalysisResults/DeliveryTimes.txt", true));
            writer.append(data);
            writer.close();
        }catch(IOException e){
            System.out.println("Problem writing to DeliveryTimes.txt");;
        }
    }

    /**
     * Creates a new DeliverTimes.txt file to be written to.
     */
    private void createAnalysisFile() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("AnalysisResults/DeliveryTimes.txt"));
        writer.write("");
        writer.close();
        analysisFileIsCreated = true;
    }

    /**
     * Calculates delivery time given the received and sent timestamp given in message
     * content.
     * @param messageContent contains received and sent timestamp data.
     */
    private long calculateDeliveryTime(MessageContent messageContent){
        long deliveryTime = -1;

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH.mm.ss.SSS");

        String timestampSent = messageContent.getTimestampSent();
        String timestampReceived = messageContent.getTimestampReceived();

        try{
            Date dateForTimestampSent = dateFormat.parse(timestampSent);
            Date dateForTimestampReceived = dateFormat.parse(timestampReceived);

            deliveryTime = (dateForTimestampReceived.getTime() - dateForTimestampSent.getTime());

        }catch(java.text.ParseException e){
            System.out.println("Error extracting timestamp data");
        }

        return deliveryTime;
    }

    /**
     * Entry point for the server, accepts port number arguments. Otherwise, it will
     * launch the server with a default port 1234.
     * @param args
     * @throws java.lang.Exception
     */
    public static void main (String[] args) throws java.lang.Exception{
        int portNumber = 1234;

        if(args.length != 0){
            portNumber = Integer.parseInt(args[0]);
        }

        ChatServer chatServer = new ChatServer(portNumber);
    }
}
