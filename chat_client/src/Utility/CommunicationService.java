package Utility;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Provides encapsulated method for establishing the socket and
 * ObjectOutputStream & ObjectInputStream to a specified host and host port.
 * Used to reach out to a server and wait for a response.
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class CommunicationService {

    private Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    /**
     * Public constructor for a CommunicationService, establishing the socket
     * and ObjectOutputStream & ObjectInputStream to a specified host and
     * host port.
     * @param hostName String specifying the host name or IP address of an
     *                 already-established server
     * @param port     int specifying the port for the already-established
     *                 server
     */
    public CommunicationService(String hostName, int port) throws IOException {

        socket = new Socket(hostName, port);
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());

    }

    /**
     * Sends a message through this communication service's output stream,
     * then waits for a response. The response message is then returned.
     * @param message message to be sent through output stream
     * @return reply message
     */
    public Message sendMessage(Message message) {
        Message msg = null;

        try {
            out.writeObject(message);
            out.flush();
            msg = (Message) in.readObject();
            socket.close();
        } catch (ClassNotFoundException | IOException e) {
            System.out.println("Trouble communicating with server.");
        }

        return msg;
    }
}
