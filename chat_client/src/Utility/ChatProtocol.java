package Utility;

/**
 * Provides an interface for more specific protocols to use as an outline. The ChatServer
 * implements it's own version of a ChatProtocol.
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public interface ChatProtocol {

    /**
     * Opens a message, executes a set of instructions based on the message's
     * content, then returns a reply message to be sent back to the original
     * sender.
     * @param message received message that will be read
     * @return reply message that will be sent to the original sender in
     * response to the received message
     */
    Message handleMessage(Message message);

}
