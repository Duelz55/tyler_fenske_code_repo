package ChatClient;

/**
 * A ChatUpdater is run as it's own thread by a ChatClient when it is born. Since the server cannot reach out
 * to a client when a new message comes in that is addressed to them, the ChatUpdater will constantly poll
 * the server for new messages.
 * @author Tyler Fenske
 * @author Tony Nguyen
 */
public class ChatUpdater implements Runnable {

    private ChatClient chatClient;

    /**
     * Constructor for a ChatClient. The ChatClient is needed to have the ChatUpdater's request for the next
     * queued message routed to the server.
     * @param chatClient a reference to the chatClient this ChatUpdater requests updated messages for.
     */
    public ChatUpdater(ChatClient chatClient){
        this.chatClient = chatClient;
    }

    /**
     * Necessary run method for a runnable object. Simply requests the next queued chat message from the server
     * constantly for the entirety of it's life.
     */
    @Override
    public void run() {
        while(true){
            chatClient.getNextQueuedChatMessage();
        }
    }
}
