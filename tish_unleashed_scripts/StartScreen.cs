﻿using UnityEngine;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
    [SerializeField] Sprite[] titles;
    [SerializeField] AudioClip eyeSound;

    SpriteRenderer spriteRenderer;
    Timer timer;

    int titleIndex = 0;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        timer = GetComponent<Timer>();
        UpdateSprite();
        Cursor.visible = true;
        Cursor.SetCursor(default, default, CursorMode.ForceSoftware);
    }

    private void Update()
    {
        if (timer.GetTimerCompleted())
        {
            UpdateTitleIndex();
            UpdateSprite();
        }
    }

    private void UpdateTitleIndex()
    {
        if (titleIndex <= 1)
        {
            titleIndex++;
        }
        else
        {
            titleIndex = 0;
        }
    }

    private void UpdateSprite()
    {
        spriteRenderer.sprite = titles[titleIndex];
        timer.StartRealTimeTimer();
    }

    public void DisableOtherButtons()
    {
        Button[] buttons = FindObjectsOfType<Button>();

        foreach(Button button in buttons)
        {
            button.interactable = false;
        }
    }

    public void PlayEyeSound()
    {
        AudioSource.PlayClipAtPoint(eyeSound, Camera.main.transform.position);
    }
}
