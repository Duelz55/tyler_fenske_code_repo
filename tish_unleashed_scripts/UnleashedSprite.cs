﻿using UnityEngine;

public class UnleashedSprite : MonoBehaviour
{
    [SerializeField] Sprite normalSprite;
    [SerializeField] Sprite unleashedSprite;

    GameState gameState;
    SpriteRenderer spriteRenderer;

    bool isUnleashed = false;

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        UpdateSprite();
    }

    private void UpdateSprite()
    {
        if (PlayerIsUnleashed())
        {
            isUnleashed = true;
            spriteRenderer.sprite = unleashedSprite;
        }
        else if (PlayerIsNotUnleashed())
        {
            isUnleashed = false;
            spriteRenderer.sprite = normalSprite;
        }
    }

    private bool PlayerIsNotUnleashed()
    {
        return !gameState.GetIsUnleashed() && isUnleashed;
    }

    private bool PlayerIsUnleashed()
    {
        return gameState.GetIsUnleashed() && !isUnleashed;
    }
}
