﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TishUnleashed : MonoBehaviour
{
    GameState gameState;
    SpriteRenderer spriteRenderer; 
    // Start is called before the first frame update

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        gameState = FindObjectOfType<GameState>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameState.GetIsUnleashed())
        {
            spriteRenderer.enabled = true;
        }
        else
        {
            spriteRenderer.enabled = false;
        }
    }
}
