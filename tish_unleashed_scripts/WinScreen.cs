﻿using UnityEngine;

public class WinScreen : MonoBehaviour
{
    [SerializeField] Sprite hardModeSprite;
    [SerializeField] Sprite normalModeSprite;
    [SerializeField] Sprite easyModeSprite;

    GameState gameState;
    AudioPlayer audioPlayer;
    SceneLoader sceneLoader;
    SpriteRenderer spriteRenderer;
    BackgroundMusic backgroundMusic;

    private void Start()
    {
        audioPlayer = FindObjectOfType<AudioPlayer>();
        sceneLoader = FindObjectOfType<SceneLoader>();
        gameState = FindObjectOfType<GameState>();
        backgroundMusic = FindObjectOfType<BackgroundMusic>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        if (gameState.GetIsHardMode())
        {
            spriteRenderer.sprite = hardModeSprite;
        }
        else if (gameState.GetIsEasyMode())
        {
            spriteRenderer.sprite = easyModeSprite;
        }
        else
        {
            spriteRenderer.sprite = normalModeSprite;
        }

        backgroundMusic.PlayWinMusic();
        Cursor.visible = true;
        Cursor.SetCursor(default, default, CursorMode.ForceSoftware);
    }

    public void ClickHarvestMoreSouls()
    {
        audioPlayer.SetButtonPressSound(.09f);
        audioPlayer.PlayButtonPressSound();
        audioPlayer.ResetAllSounds();
        sceneLoader.LoadFirstSceneWithTimer();
    }
}
