﻿using UnityEngine;

public class LoseCollider : MonoBehaviour
{
    GameState gameState;
    Level level;

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
        level = FindObjectOfType<Level>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "PlayerBall")
        {
            if (AtLeastTwoBallsStillInPlay())
            {
                DestroyBall(collision);
            }
            else
            {
                DecrementPlayerSoulsAndResetBallIfApplicable(collision.gameObject.GetComponent<Ball>());
            }
        }
        else
        {
            Destroy(collision.gameObject);
        }
    }

    private void DecrementPlayerSoulsAndResetBallIfApplicable(Ball ball)
    {
        gameState.RemovePlayerSoul();

        if (PlayerOutOfSouls())
        {
            gameState.SetUnleashedReady(false);
            level.SendToGameOver();
        }
        else
        {
            ball.LockBallToPaddle();
        }
    }

    private bool AtLeastTwoBallsStillInPlay()
    {
        return level.GetNumBalls() > 1;
    }

    private void DestroyBall(Collider2D collision)
    {
        level.DestroyBall();
        Destroy(collision.gameObject);
    }

    private bool PlayerOutOfSouls()
    {
        return gameState.GetPlayerSouls() <= 0;
    }
}

