﻿using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    [SerializeField] AudioClip fire;
    [Range(0.0f, 1f)] [SerializeField] float fireVolume = 1f;
    [SerializeField] AudioClip smoke;
    [Range(0.0f, 1f)] [SerializeField] float smokeVolume = 1f;
    [SerializeField] AudioClip time;
    [Range(0.0f, 1f)] [SerializeField] float timeVolume = 1f;
    [SerializeField] AudioClip soul;
    [Range(0.0f, 1f)] [SerializeField] float soulVolume = 1f;
    [SerializeField] AudioClip death;
    [Range(0.0f, 1f)] [SerializeField] float deathVolume = 1f;
    [SerializeField] AudioClip tableSize;
    [Range(0.0f, 1f)] [SerializeField] float tableSizeVolume = 1f;
    [SerializeField] AudioClip blockHeal;
    [Range(0.0f, 1f)] [SerializeField] float blockHealVolume = 1f;
    [SerializeField] AudioClip spawnExtraBall;
    [Range(0.0f, 1f)] [SerializeField] float spawnExtraBallVolume = 1f;
    [SerializeField] AudioClip blockBreak;
    [Range(0.0f, 1f)] [SerializeField] float blockBreakVolume = 1f;
    [SerializeField] AudioClip unleashedReady;
    [Range(0.0f, 1f)] [SerializeField] float unleashedReadyVolume = 1f;
    [SerializeField] AudioClip deathBallKill;
    [Range(0.0f, 1f)] [SerializeField] float deathBallKillVolume = 1f;
    [SerializeField] AudioClip gameOver;
    [Range(0.0f, 1f)] [SerializeField] float gameOverVolume = 1f;
    [SerializeField] AudioClip buttonPress;
    [Range(0.0f, 1f)] [SerializeField] float buttonPressVolume = 1f;

    private float originalFireVolume;
    private float originalBlockBreakVolume;
    private float originalSmokeVolume;
    private float originalTimeVolume;
    private float originalSoulVolume;
    private float originalDeathVolume;
    private float originalTableSizeVolume;
    private float originalBlockHealVolume;
    private float originalSpawnExtraBallVolume;
    private float originalUnleashedReadyVolume;
    private float originalDeathBallKillVolume;
    private float originalGameOverVolume;
    private float originalButtonPressVolume;

    private bool finishedStartup = false;
    private bool soundChangePending = false;

    private void Awake()
    {
        originalFireVolume = fireVolume;
        originalBlockBreakVolume = blockBreakVolume;
        originalSmokeVolume = smokeVolume;
        originalTimeVolume = timeVolume;
        originalSoulVolume = soulVolume;
        originalDeathVolume = deathVolume;
        originalTableSizeVolume = tableSizeVolume;
        originalBlockHealVolume = blockHealVolume;
        originalSpawnExtraBallVolume = spawnExtraBallVolume;
        originalUnleashedReadyVolume = unleashedReadyVolume;
        originalDeathBallKillVolume = deathBallKillVolume;
        originalGameOverVolume = gameOverVolume;
        originalButtonPressVolume = buttonPressVolume;

        finishedStartup = true;
    }

    public void Update()
    {
        if (soundChangePending)
        {
            soundChangePending = false;
            ResetAllSounds();
        }    
    }

    public void PlayButtonPressSound()
    {
        PlaySound(buttonPress, buttonPressVolume);
    }

    public void PlayGameOverSound()
    {
        PlaySound(gameOver, gameOverVolume);
    }

    public void PlayDeathBallKillSound()
    {
        PlaySound(deathBallKill, deathBallKillVolume);
    }

    public void PlayFireSound()
    {
        PlaySound(fire, fireVolume);
    }

    public void PlaySmokeSound()
    {
        PlaySound(smoke, smokeVolume);
    }

    public void PlayTimeSound()
    {
        PlaySound(time, timeVolume);
    }

    public void PlaySoulSound()
    {
        PlaySound(soul, soulVolume);
    }

    public void PlayDeathSound()
    {
        PlaySound(death, deathVolume);
    }

    public void PlayTableSizeSound()
    {
        PlaySound(tableSize, tableSizeVolume);
    }

    public void PlaySpawnExtraBallSound()
    {
        PlaySound(spawnExtraBall, spawnExtraBallVolume);
    }

    public void PlayBlockHealSound()
    {
        PlaySound(blockHeal, blockHealVolume);
    }

    public void PlayBlockBreakSound()
    {
        PlaySound(blockBreak, blockBreakVolume);
    }

    public void PlayUnleashedReadySound()
    {
        PlaySound(unleashedReady, unleashedReadyVolume);
    }

    public void PlayUnleashedActivatedSound()
    {
        GetComponent<AudioSource>().Play();
    }

    public void SetFireVolume(float volume)
    {
        fireVolume = volume;
    }

    public void SetSmokeVolume(float volume)
    {
        smokeVolume = volume;
    }

    public void SetBlockBreakVolume(float volume)
    {
        blockBreakVolume = volume;
    }

    public void SetButtonPressSound(float volume)
    {
        buttonPressVolume = volume;
    }

    public void SetSpawnExtraBallVolume(float volume)
    {
        spawnExtraBallVolume = volume;
    }

    public void ResetAllSounds()
    {
        if (finishedStartup)
        {
            fireVolume = originalFireVolume;
            blockBreakVolume = originalBlockBreakVolume;
            smokeVolume = originalSmokeVolume;
            timeVolume = originalTimeVolume;
            soulVolume = originalSoulVolume;
            deathVolume = originalDeathVolume;
            tableSizeVolume = originalTableSizeVolume;
            blockHealVolume = originalBlockHealVolume;
            spawnExtraBallVolume = originalSpawnExtraBallVolume;
            unleashedReadyVolume = originalUnleashedReadyVolume;
            deathBallKillVolume = originalDeathBallKillVolume;
            gameOverVolume = originalGameOverVolume;
            buttonPressVolume = originalButtonPressVolume;
        }
        else
        {
            soundChangePending = true;
        }

    }

    private void PlaySound(AudioClip clip, float volume)
    {
        AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, volume);
    }
}