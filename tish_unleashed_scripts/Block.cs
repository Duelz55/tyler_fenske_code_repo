﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Block : MonoBehaviour
{
    enum UnbreakableTag { Unbreakable, Heal, Death, Shrink };

    [SerializeField] GameObject extraBall;
    [SerializeField] GameObject blockBreakVFX;
    [SerializeField] GameObject blockHealVFX;
    [SerializeField] GameObject elementalSpawn;
    [SerializeField] GameObject elementalDespawn;
    [SerializeField] GameObject elementalVFX;
    [SerializeField] GameObject specialVFX;
    [SerializeField] Sprite[] sprites;
    [SerializeField] Sprite[] unbreakableSprites;
    [SerializeField] Sprite destroyed;
    [SerializeField] bool dancingElemental;
    [SerializeField] Vector2 elementalSpawnTime;
    [SerializeField] UnbreakableTag elementalType;

    Level level;
    GameState gameState;
    SpriteRenderer spriteRenderer;
    Sprite currentDamageLevelSprite;
    AudioPlayer audioPlayer;
    VFXPlayer vfx;
    Timer timer;

    bool timerSet;
    int timesHit;
    string startingTag;
    
    private void Awake()
    {
        timer = GetComponent<Timer>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        currentDamageLevelSprite = sprites[0];
        startingTag = tag;
        timerSet = false;
    }

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
        audioPlayer = FindObjectOfType<AudioPlayer>();
        vfx = FindObjectOfType<VFXPlayer>();
        level = FindObjectOfType<Level>();
        AddToBlockCountIfBreakable();
    }

    private void Update()
    {
        UpdateSprite();
        UpdateBreakableStatus();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Ball collidingBall = collision.gameObject.GetComponent<Ball>();
        if (collidingBall.tag == "PlayerBall")
        {
            HandleHit();
        }
    }

    private void UpdateBreakableStatus()
    {
        if (dancingElemental)
        {
            if (!timerSet)
            {
                timerSet = true;
                float timeToWait = Random.Range(elementalSpawnTime.x, elementalSpawnTime.y);
                timer.StartGameTimer(timeToWait);
            }
            else if (timer.GetTimerCompleted())
            {
                timerSet = false;
                SwapBreakable();
            }
        }
    }

    private void HandleHit()
    {
        if (IsBreakableBlock())
        {
            HandleBreakable();
        }
        else
        {
            HandleUnbreakable();
        }
    }

    private void HandleBreakable()
    {
        timesHit++;
        if (timesHit >= sprites.Length)
        {
            DestroyBlock();
        }
        else
        {
            UpdateSpriteWithNextDamageLevel();
        }
    }

    public void DestroyBlock()
    {
        audioPlayer.PlayBlockBreakSound();
        vfx.TriggerVFX(blockBreakVFX, transform.position);
        spriteRenderer.sprite = destroyed;
        GetComponent<Collider2D>().isTrigger = true;
        //gameState.AddPointsForDestroyedBlock();
        HandleSpecialBlocks();
        level.DestroyBlock();
        timesHit = 0;
        tag = "Destroyed";
    }

    public void ObliterateBlock()
    {
        audioPlayer.PlayBlockBreakSound();
        vfx.TriggerVFX(blockBreakVFX, transform.position);
        spriteRenderer.sprite = destroyed;
        GetComponent<Collider2D>().isTrigger = true;
        tag = "Obliterated";
    }

    private void UpdateSpriteWithNextDamageLevel()
    {
        currentDamageLevelSprite = sprites[timesHit];
        spriteRenderer.sprite = currentDamageLevelSprite;
    }

    private void HandleUnbreakable()
    {
        HandleHealElemental();
        HandleShrinkElemental();
        HandleDeathElemental();
    }

    private void HandleSpecialBlocks()
    {
        HandleSoulBlockDestroyed();
        HandleTimeBlockDestroyed();
        HandleTableSizeBlockDestroyed();
        HandleSpawnExtraBallBlockDestroyed();
    }

    private void HandleTimeBlockDestroyed()
    {
        if(tag == "Slow")
        {
            gameState.SafelySetGameSpeed(gameState.GetGameSpeed() - .25f);
            vfx.TriggerVFX(specialVFX, transform.position);
            audioPlayer.PlayTimeSound();
        }
        else if(tag == "Fast")
        {
            gameState.SafelySetGameSpeed(gameState.GetGameSpeed() + .25f);
            vfx.TriggerVFX(specialVFX, transform.position);
            audioPlayer.PlayTimeSound();
        }
    }

    private void HandleSoulBlockDestroyed()
    {
        if (tag == "Soul")
        {
            gameState.AddPlayerSoul();
            vfx.TriggerVFX(specialVFX, transform.position);
            audioPlayer.PlaySoulSound();
        }
        else if(tag == "Soul2")
        {
            gameState.AddPlayerSoul();
            gameState.AddPlayerSoul();
            vfx.TriggerVFX(specialVFX, transform.position, loop:2);
            audioPlayer.PlaySoulSound();
        }
    }

    private void HandleShrinkElemental()
    {
        if (tag == "Shrink")
        {
            FindObjectOfType<Paddle>().MakePaddleSmaller();
        }
    }

    private void HandleHealElemental()
    {
        if (tag == "Heal")
        {
            Block[] blocks = FindObjectsOfType<Block>();

            List<Block> l_blocks = new List<Block>();
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].tag == "Destroyed")
                {
                    l_blocks.Add(blocks[i]);
                }
            }

            if (l_blocks.Any())
            {
                int randomNum = Random.Range(0, l_blocks.Count);
                Block block = l_blocks.ElementAt(randomNum);
                block.HealBlock();
            }
        }
    }

    private void HandleDeathElemental()
    {
        if (tag == "Death")
        {
            SpawnDeathBall();
        }
    }

    public void SpawnDeathBall()
    {
        Vector3 placeToSpawn = transform.position;
        placeToSpawn.y -= transform.localScale.y;
        vfx.TriggerVFX(elementalVFX, placeToSpawn);
        audioPlayer.PlayDeathSound();
        placeToSpawn.y -= .5f * extraBall.transform.localScale.x;
        Instantiate(extraBall, placeToSpawn, Quaternion.identity);
    }

    public void HealBlock()
    {
        vfx.TriggerVFX(blockHealVFX, transform.position);
        audioPlayer.PlayBlockHealSound();
        level.AddBlock();
        currentDamageLevelSprite = sprites[0];
        tag = "Breakable";
        startingTag = "Breakable";
        GetComponent<Collider2D>().isTrigger = false;
    }

    private void HandleTableSizeBlockDestroyed()
    {
        if(tag == "Grow")
        {
            FindObjectOfType<Paddle>().MakePaddleBigger();
            vfx.TriggerVFX(specialVFX, transform.position);
            audioPlayer.PlayTableSizeSound();
        }
    }

    private void HandleSpawnExtraBallBlockDestroyed()
    {
        if(tag == "Spawn")
        {
            Instantiate(extraBall, new Vector3 (FindObjectOfType<Paddle>().transform.position.x, 1.23f, 0), Quaternion.identity);
            vfx.TriggerVFX(specialVFX, transform.position);
            audioPlayer.PlaySpawnExtraBallSound();
        }
    }

    private void AddToBlockCountIfBreakable()
    {
        if (IsBreakableBlock())
        {
            level.AddBlock();
        }
    }

    private void UpdateSprite()
    {
        if (IsBreakableBlock())
        {
            spriteRenderer.sprite = currentDamageLevelSprite;
        }
        else if (IsUnbreakable())
        {
            spriteRenderer.sprite = unbreakableSprites[timesHit];
        }
    }

    public void MakeUnbreakable()
    {
        tag = elementalType.ToString();
        vfx.TriggerVFX(elementalSpawn, transform.position);
        audioPlayer.PlayFireSound();
    }

    public void MakeBreakable()
    {
        tag = startingTag;
        vfx.TriggerVFX(elementalDespawn, transform.position);
        audioPlayer.PlaySmokeSound();
    }

    public void SwapBreakable()
    {
        if (IsBreakableBlock())
        {
            MakeUnbreakable();
        }
        else if (tag != "Destroyed")
        {
            MakeBreakable();
        }
    }

    public bool IsUnbreakable()
    {
        return (tag == "Unbreakable" || tag == "Heal" || tag == "Death" || tag == "Shrink");
    }

    public bool IsBreakableBlock()
    {
        return (!IsUnbreakable() && tag != "Destroyed" && tag != "Obliterated");
    }

    public bool GetIsDancing()
    {
        return dancingElemental;
    }
}
