﻿using UnityEngine;
using TMPro;


public class GameState : MonoBehaviour
{
    [Range(0.1f, 10f)] [SerializeField] float gameSpeed;

    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI manaText;
    [SerializeField] GameObject manaDestroyFX;
    [SerializeField] int playerStartingSouls;
    [SerializeField] int playerSouls;
    [SerializeField] bool autoPlayEnabled;
    [SerializeField] bool developerMode;

    const int DESTROYED_BLOCK_POINTS = 1;

    Vector3 manaDestroyCoords = new Vector3(1.11f, 11.14f, -8f);
    float startingGameSpeed;
    int score = 0;
    int unleashedScore = 0;
    int lastCheckpointSceneIndex = 0;
    bool isUnleashed = false;
    bool unleashedReady = false;
    bool isHardMode = false;
    bool isEasyMode = false;
    bool checkpointReloaded = false;


    private void Awake()
    {
        int numGameStates = FindObjectsOfType<GameState>().Length;
        
        if(numGameStates > 1)
        {
            gameObject.SetActive(false);
            DestroyGameState();
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }

        startingGameSpeed = gameSpeed;
        InitializePlayerSouls();
    }

    void Update()
    {
        Time.timeScale = gameSpeed;
        UpdateSoulText();
    }

    public void ResetGameSpeed()
    {
        gameSpeed = startingGameSpeed;
    }

    public void InitializePlayerSouls()
    {
        playerSouls = playerStartingSouls;
    }

    public void DestroyGameState()
    {
        Destroy(gameObject);
    }

    private void UpdateSoulText()
    {
        if (FindObjectOfType<Level>() != null)
        {
            manaText.SetText(playerSouls.ToString());
        }
        else
        {
            manaText.SetText("");
        }
    }

    public void AddPlayerSoul()
    {
        playerSouls++;
    }

    public void RemovePlayerSoul()
    {
        playerSouls--;
        Destroy(Instantiate(manaDestroyFX, manaDestroyCoords, transform.rotation), 1f);
    }

    public void ResetScores()
    {
        score = 0;
        unleashedScore = 0;
    }

    public int GetPlayerSouls()
    {
        return playerSouls;
    }

    public void SetGameSpeed(float gameSpeed)
    {
        this.gameSpeed = gameSpeed;
    }

    public void SafelySetGameSpeed(float gameSpeed)
    {
        if (!isUnleashed)
        {
            this.gameSpeed = Mathf.Clamp(gameSpeed, .5f, 1.5f);
        }
    }

    public float GetGameSpeed()
    {
        return gameSpeed;
    }

    public float GetStartingGameSpeed()
    {
        return startingGameSpeed;
    }

    public bool GetUnleashedReady()
    {
        return unleashedReady;
    }

    public void SetUnleashedReady(bool unleashedReady)
    {
        this.unleashedReady = unleashedReady;
    }

    public bool GetIsUnleashed()
    {
        return isUnleashed;
    }

    public void SetIsUnleashed(bool isUnleashed)
    {
        this.isUnleashed = isUnleashed;
    }

    public void SetAutoPlay(bool autoPlay)
    {
        autoPlayEnabled = autoPlay;
    }

    public bool IsAutoPlayEnabled()
    {
        return autoPlayEnabled;
    }

    public int GetUnleashedScore()
    {
        return unleashedScore;
    }

    public bool GetIsInDeveloperMode()
    {
        return developerMode;
    }

    public void SetLastCheckpointSceneIndex(int sceneIndex)
    {
        if (!isHardMode)
        {
            lastCheckpointSceneIndex = sceneIndex;
        }
        else
        {
            lastCheckpointSceneIndex = 1;
        }
    }

    public void SetToHardMode()
    {
        isHardMode = true;
    }

    public void SetToEasyMode()
    {
        playerStartingSouls = 5;
        InitializePlayerSouls();
        startingGameSpeed = .75f;
        this.gameSpeed = startingGameSpeed;
        isEasyMode = true;
    }

    public int GetLastCheckpointSceneIndex()
    {
        return lastCheckpointSceneIndex;
    }

    public bool GetIsDeveloperMode()
    {
        return developerMode;
    }

    public bool GetCheckpointReloaded()
    {
        return checkpointReloaded;
    }

    public void SetCheckpointReloaded(bool checkpointReloaded)
    {
        this.checkpointReloaded = checkpointReloaded;
    }

    public bool GetIsEasyMode()
    {
        return isEasyMode;
    }

    public bool GetIsHardMode()
    {
        return isHardMode;
    }

    public void TurnOnDeveloperMode()
    {
        developerMode = true;
    }
}
