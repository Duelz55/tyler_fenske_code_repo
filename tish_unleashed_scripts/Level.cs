﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class Level : MonoBehaviour
{
    [SerializeField] int numBlocks;
    [SerializeField] int numBalls;
    [SerializeField] GameObject gameOverPrefab;
    [SerializeField] bool isCheckpoint = false;
    [SerializeField] bool giveUnleashed = false;
    [SerializeField] bool isBossLevel = false;

    SceneLoader sceneLoader;
    GameState gameState;
    Unleashed unleashed;
    Timer timer;

    bool hasStarted;
    bool readyForNextScene = false;
    bool readyForGameOverScene = false;

    private void Awake()
    {
        timer = GetComponent<Timer>();
    }

    private void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
        gameState = FindObjectOfType<GameState>();
        unleashed = FindObjectOfType<Unleashed>();
        FindObjectOfType<Canvas>().GetComponent<CanvasSetup>().UpdateCanvas();
        gameState.SetGameSpeed(gameState.GetStartingGameSpeed());
        DocumentCheckpointStatus();
        GiveUnleashedIfApplicable();
        FindObjectOfType<AudioPlayer>().ResetAllSounds();
        Cursor.visible = false;
        hasStarted = true;
        SwitchMusicIfNeeded();
    }

    private void Update()
    {
        if (readyForNextScene)
        {
            if (timer.GetTimerCompleted())
            {
                SwitchBackToNormalMusicIfBossDefeated();
                sceneLoader.LoadNextScene();
            }
        }
        else if (readyForGameOverScene)
        {
            DestroyAllBalls();
            if (timer.GetTimerCompleted())
            {
                sceneLoader.LoadGameOverScene();
            }
        }

        if (gameState.GetCheckpointReloaded())
        {
            gameState.SetCheckpointReloaded(false);

            if (!isBossLevel)
            {
                FindObjectOfType<BackgroundMusic>().PlayNormalMusic();
            }
        }
    }

    private void SwitchBackToNormalMusicIfBossDefeated()
    {
        if (isBossLevel)
        {
            FindObjectOfType<BackgroundMusic>().PlayNormalMusic();
        }
    }

    private void SwitchMusicIfNeeded()
    {
        if (isBossLevel)
        {
            FindObjectOfType<BackgroundMusic>().PlayBossMusic();
        }
    }

    private void GiveUnleashedIfApplicable()
    {
        if (giveUnleashed && !gameState.GetUnleashedReady())
        {
            gameState.SetUnleashedReady(true);
            unleashed.NotifyUnleashedReady();
        }
    }

    public void AddBlock()
    {
        numBlocks++;
    }

    public void DestroyBlock()
    {
        numBlocks--;
        if(numBlocks <= 0)
        {
            hasStarted = false;
            bool needToResetMusic = gameState.GetIsUnleashed();
            unleashed.DeactivateUnleashed(needToResetMusic);
            timer.StartRealTimeTimer();
            readyForNextScene = true;
        }
    }

    public void SendToGameOver()
    {
        hasStarted = false;
        unleashed.DeactivateUnleashed();
        FindObjectOfType<AudioPlayer>().PlayGameOverSound();
        timer.StartRealTimeTimer();
        Instantiate(gameOverPrefab, new Vector3(8, 6, 0), Quaternion.identity);
        FindObjectOfType<BackgroundMusic>().PlayGameOverMusic();
        readyForGameOverScene = true;
    }

    private void DestroyAllBalls()
    {
        Ball[] balls = FindObjectsOfType<Ball>();

        for (int i = 0; i < balls.Length; i++)
        {
            Destroy(balls[i].gameObject);
        }
    }

    private void DocumentCheckpointStatus()
    {
        if (isCheckpoint)
        {
            gameState.SetLastCheckpointSceneIndex(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void AddBall()
    {
        numBalls++;
    }

    public void DestroyBall()
    {
        numBalls--;
    }

    public int GetNumBalls()
    {
        return numBalls;
    }

    public bool GetHasStarted()
    {
        return hasStarted;
    }

    public bool GetIsCheckpoint()
    {
        return isCheckpoint;
    }

    public bool GetIsBossLevel()
    {
        return isBossLevel;
    }

    public bool GetLevelHasEnded()
    {
        return readyForGameOverScene || readyForNextScene;
    }
}
