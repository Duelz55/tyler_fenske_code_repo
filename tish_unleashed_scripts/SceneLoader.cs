﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.SceneManagement;

public class SceneLoader : UnityEngine.MonoBehaviour
{
    Timer timer;
    bool loadNextScene = false;
    bool loadFirstScene = false;
    bool loadCheckpoint = false;

    private void Start()
    {
        timer = GetComponent<Timer>();
    }

    private void Update()
    {
        if (timer.GetTimerCompleted())
        {
            if (loadNextScene)
            {
                LoadNextScene();
            }
            else if (loadFirstScene)
            {
                LoadFirstScene();
            }
            else if (loadCheckpoint)
            {
                LoadLastCheckpoint();
            }
        }
    }

    public void LoadFirstSceneWithTimer()
    {
        loadFirstScene = true;
        timer.StartRealTimeTimer();
    }

    public void LoadNextSceneWithTimer()
    {
        loadNextScene = true;
        timer.StartRealTimeTimer();
    }

    public void LoadCheckpointWithTimer()
    {
        loadCheckpoint = true;
        timer.StartRealTimeTimer();
    }

    public void LoadNextScene()
    {
        int currentSceneIndex = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
        UnityEngine.SceneManagement.SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void LoadFirstScene()
    {
        FindObjectOfType<GameState>().DestroyGameState();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void LoadLastCheckpoint()
    {
        GameState gameState = FindObjectOfType<GameState>();
        int checkpoint = gameState.GetLastCheckpointSceneIndex();

        if(checkpoint != 0)
        {
            gameState.InitializePlayerSouls();
            UnityEngine.SceneManagement.SceneManager.LoadScene(checkpoint);
        }
        else
        {
            LoadFirstScene();
        }
    }

    public void ReloadCurrentLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadGameOverScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game Over");
    }

    public void QuitGame()
    {
        UnityEngine.Application.Quit();
    }
}
