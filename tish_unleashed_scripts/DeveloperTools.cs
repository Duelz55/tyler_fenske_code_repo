﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class DeveloperTools : MonoBehaviour
{
    GameState gameState;

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
    }

    private void Update()
    {
        if (gameState.GetIsInDeveloperMode())
        {
            ChangeTableSize();

            HealRandomBlock();

            MakeRandomBlocksUnbreakableOrBreakable();

            SpawnRandomDeathBall();

            MakeUnleashedReady();

            DefeatLevel();
        }
        else if (Input.GetKeyDown(KeyCode.T) &&
                 Input.GetKeyDown(KeyCode.Y))
        {
                gameState.TurnOnDeveloperMode();
                
        }
    }

    private static void MakeRandomBlocksUnbreakableOrBreakable()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Block[] blocks = FindObjectsOfType<Block>();

            List<Block> l_blocks = new List<Block>();
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].tag != "Destroyed")
                {
                    l_blocks.Add(blocks[i]);
                }
            }

            if (l_blocks.Any())
            {
                int randomNum = Random.Range(0, l_blocks.Count);
                Block block = l_blocks.ElementAt(randomNum);
                block.SwapBreakable();
            }
        }
    }

    private static void HealRandomBlock()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Block[] blocks = FindObjectsOfType<Block>();

            List<Block> l_blocks = new List<Block>();
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].tag == "Destroyed")
                {
                    l_blocks.Add(blocks[i]);
                }
            }

            if (l_blocks.Any())
            {
                int randomNum = Random.Range(0, l_blocks.Count);
                Block block = l_blocks.ElementAt(randomNum);
                block.HealBlock();
            }
        }
    }

    private void ChangeTableSize()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            FindObjectOfType<Paddle>().MakePaddleSmaller();
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            FindObjectOfType<Paddle>().MakePaddleBigger();
        }
    }

    private void SpawnRandomDeathBall()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            Block[] blocks = FindObjectsOfType<Block>();

            List<Block> l_blocks = new List<Block>();
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].tag != "Destroyed")
                {
                    l_blocks.Add(blocks[i]);
                }
            }

            if (l_blocks.Any())
            {
                int randomNum = Random.Range(0, l_blocks.Count);
                Block block = l_blocks.ElementAt(randomNum);
                block.SpawnDeathBall();
            }
        }
    }

    private void MakeUnleashedReady()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            FindObjectOfType<GameState>().SetUnleashedReady(true);
            FindObjectOfType<Unleashed>().NotifyUnleashedReady();
        }
    }

    private void DefeatLevel()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Block[] blocks = FindObjectsOfType<Block>();

            FindObjectOfType<AudioPlayer>().SetBlockBreakVolume(.001f);
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].IsBreakableBlock() || blocks[i].GetIsDancing())
                {
                    blocks[i].DestroyBlock();
                }
            }
        }
    }
}
