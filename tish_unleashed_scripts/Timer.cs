﻿using System.Collections;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] float seconds;

    GameState gameState;
    bool timerCompleted = false;

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
    }

    public void StartRealTimeTimer()
    {
        StartCoroutine(RealTimeWait());
    }

    private IEnumerator RealTimeWait()
    {
        yield return new WaitForSeconds(seconds * gameState.GetGameSpeed());
        timerCompleted = true;
    }

    public void StartGameTimer(float seconds)
    {
        StartCoroutine(GameWait(seconds));
    }

    private IEnumerator GameWait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        timerCompleted = true;
    }

    public bool GetTimerCompleted()
    {
        if (timerCompleted)
        {
            timerCompleted = false;
            return true;
        }
        return false;
    }
}
