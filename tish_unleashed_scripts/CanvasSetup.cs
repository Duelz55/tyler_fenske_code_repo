﻿using UnityEngine;

public class CanvasSetup : MonoBehaviour
{
    Canvas canvas;

    private void Awake()
    {
        canvas = GetComponent<Canvas>();
    }

    private void Start()
    {
        UpdateCanvas();
    }

    public void UpdateCanvas()
    {
        canvas.worldCamera = FindObjectOfType<Camera>();
    }
}
