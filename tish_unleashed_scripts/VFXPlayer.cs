﻿using UnityEngine;

public class VFXPlayer : MonoBehaviour
{
    public void TriggerVFX(GameObject vfx, Vector3 destroyPosition, int depth = -3, int loop = 1)
    {
        destroyPosition.z = depth;

        for(int i = 0; i < loop; i++)
        {
            Destroy(Instantiate(vfx, destroyPosition, Quaternion.identity), 1f);
        }
    }
}
