﻿using UnityEngine;

public class GameOver : MonoBehaviour
{
    GameState gameState;
    AudioPlayer audioPlayer;
    SceneLoader sceneLoader;

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
        audioPlayer = FindObjectOfType<AudioPlayer>();
        sceneLoader = FindObjectOfType<SceneLoader>();
        Cursor.visible = true;
        Cursor.SetCursor(default, default, CursorMode.ForceSoftware);
    }

    public void ClickCheckpoint()
    {
        gameState.SetCheckpointReloaded(true);
        audioPlayer.PlayButtonPressSound();
        sceneLoader.LoadCheckpointWithTimer();
    }

    public void ClickNewGame()
    {
        audioPlayer.PlayButtonPressSound();
        sceneLoader.LoadFirstSceneWithTimer();
    }
}
