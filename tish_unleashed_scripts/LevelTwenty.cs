﻿using UnityEngine;

public class LevelTwenty : MonoBehaviour
{
    [SerializeField] float customFireVolume;
    [SerializeField] float customSmokeVolume;
    [SerializeField] float customBallVolume;

    AudioPlayer audioPlayer;
    Level level;
    AudioSource ballAudioSource;

    float originalBallVolume;
    int numBalls = 1;
    bool hasChangedVolume = false;

    private void Start()
    {
        audioPlayer = FindObjectOfType<AudioPlayer>();
        level = FindObjectOfType<Level>();
        ballAudioSource = FindObjectOfType<Ball>().GetComponent<AudioSource>();

        originalBallVolume = ballAudioSource.volume;
    }

    private void Update()
    {
        if (!hasChangedVolume)
        {
            audioPlayer.SetFireVolume(customFireVolume);
            audioPlayer.SetSmokeVolume(customSmokeVolume);
            hasChangedVolume = true;
        }

        Ball[] balls = FindObjectsOfType<Ball>();

        if (numBalls != balls.Length && balls.Length >= 13)
        {
            numBalls = balls.Length;
            UpdateBallSounds(balls);
        }
        else if (numBalls != balls.Length)
        {
            numBalls = balls.Length;
            ResetBallVolume(balls);
        }
    }

    private void ResetBallVolume(Ball[] balls)
    {
        foreach (Ball ball in balls)
        {
            ball.GetComponent<AudioSource>().volume = originalBallVolume;
        }
    }

    private void UpdateBallSounds(Ball[] balls)
    {
        foreach (Ball ball in balls)
        {
            ball.GetComponent<AudioSource>().volume = customBallVolume;
        }
    }
}
