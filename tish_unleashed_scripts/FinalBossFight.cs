﻿using UnityEngine;

public class FinalBossFight : MonoBehaviour
{
    [SerializeField] Block redBoss;
    [SerializeField] Block[] redWall;
    [SerializeField] Block[] redMinions;

    [SerializeField] Block blackBoss;
    [SerializeField] Block[] blackWall;
    [SerializeField] Block[] blackMinions;

    [SerializeField] Block blueBoss;
    [SerializeField] Block[] blueWall;
    [SerializeField] Block[] blueMinions;

    [SerializeField] Block pinkBoss;
    [SerializeField] Block[] pinkWall;
    [SerializeField] Block[] pinkMinions;

    [SerializeField] Ball[] enemyBalls;
    [SerializeField] GameObject enemyBallObliterateVFX;

    VFXPlayer vfx;
    AudioPlayer audioPlayer;
    GameState gameState;

    bool redBossAlive = true;
    bool blackBossAlive = true;
    bool blueBossAlive = true;
    bool pinkBossAlive = true;

    bool redMinionsAlive = true;
    bool blackMinionsAlive = true;
    bool blueMinionsAlive = true;
    bool pinkMinionsAlive = true;

    private void Start()
    {
        vfx = FindObjectOfType<VFXPlayer>();
        audioPlayer = FindObjectOfType<AudioPlayer>();
        gameState = FindObjectOfType<GameState>();
    }

    private void Update()
    {
        DestroyWallIfAllMinionsOfColorDestroyed();

        if (BossOfColorDestroyed(redBoss) && redBossAlive)
        {
            redBossAlive = false;
            ObliterateEnemyBalls();
        }

        if (BossOfColorDestroyed(blueBoss) && blueBossAlive)
        {
            blueBossAlive = false;
            RestorePaddle();
        }

        if (BossOfColorAlive(blueBoss) && !blueBossAlive)
        {
            blueBossAlive = true;
        }

        DeveloperTools();
    }

    private void DestroyWallIfAllMinionsOfColorDestroyed()
    {
        if (AllMinionsOfColorDestroyed(redMinions) && redMinionsAlive)
        {
            redMinionsAlive = false;
            OblierateWall(redWall);
        }
        else if (AllMinionsOfColorDestroyed(blackMinions) && blackMinionsAlive)
        {
            blackMinionsAlive = false;
            OblierateWall(blackWall);
            LaunchEnemyBalls();
        }
        else if (AllMinionsOfColorDestroyed(blueMinions) && blueMinionsAlive)
        {
            blueMinionsAlive = false;
            OblierateWall(blueWall);
        }
        else if (AllMinionsOfColorDestroyed(pinkMinions) && pinkMinionsAlive)
        {
            pinkMinionsAlive = false;
            OblierateWall(pinkWall);
        }
    }

    private bool AllMinionsOfColorDestroyed(Block[] blocks)
    {
        for (int i = 0; i < blocks.Length; i++)
        {
            if(blocks[i].gameObject.tag != "Destroyed")
            {
                return false;
            }
        }

        return true;
    }

    private void OblierateWall(Block[] wall)
    {
        FindObjectOfType<AudioPlayer>().SetBlockBreakVolume(.03f);
        for (int i = 0; i < wall.Length; i++)
        {
            wall[i].ObliterateBlock();
        }
        FindObjectOfType<AudioPlayer>().SetBlockBreakVolume(.565f);
    }

    private void ObliterateEnemyBalls()
    {
        for (int i = 0; i < enemyBalls.Length; i++)
        {
            vfx.TriggerVFX(enemyBallObliterateVFX, enemyBalls[i].transform.position);
            audioPlayer.PlayFireSound();
            Destroy(enemyBalls[i].gameObject);
        }
    }

    private void LaunchEnemyBalls()
    {
        for (int i = 0; i < enemyBalls.Length; i++)
        {
            enemyBalls[i].LaunchBall();
        }
    }

    private void RestorePaddle()
    {
        FindObjectOfType<Paddle>().ResetPaddleSize();
    }

    private bool BossOfColorDestroyed(Block boss)
    {
        return boss.gameObject.tag == "Destroyed";
    }

    private bool BossOfColorAlive(Block boss)
    {
        return boss.gameObject.tag == "Breakable";
    }

    private void DestroyMinionsOfColor(Block[] blocks)
    {
        for (int i = 0; i < blocks.Length; i++)
        {
            blocks[i].DestroyBlock();
        }
    }

    private void DeveloperTools()
    {
        if (gameState.GetIsDeveloperMode())
        {
            if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                DestroyMinionsOfColor(pinkMinions);
            }
            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                DestroyMinionsOfColor(blueMinions);
            }
            if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                DestroyMinionsOfColor(blackMinions);
            }
            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                DestroyMinionsOfColor(redMinions);
            }
        }
    }
}
