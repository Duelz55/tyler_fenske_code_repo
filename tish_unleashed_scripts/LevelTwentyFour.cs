﻿using UnityEngine;

public class LevelTwentyFour : MonoBehaviour
{
    [SerializeField] float customFireVolume;
    [SerializeField] float customSmokeVolume;

    AudioPlayer audioPlayer;
    AudioSource ballAudioSource;

    bool hasChangedVolume = false;

    private void Start()
    {
        audioPlayer = FindObjectOfType<AudioPlayer>();
        ballAudioSource = FindObjectOfType<Ball>().GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (!hasChangedVolume)
        {
            audioPlayer.SetFireVolume(customFireVolume);
            audioPlayer.SetSmokeVolume(customSmokeVolume);
            hasChangedVolume = true;
        }
    }
}
