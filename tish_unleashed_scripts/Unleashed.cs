﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Unleashed : MonoBehaviour
{
    GameState gameState;
    Timer timer;
    Level level;
    AudioPlayer audioPlayer;

    Vector2 startingBallRandomBounds;

    private void Awake()
    {
        timer = GetComponent<Timer>();
        audioPlayer = FindObjectOfType<AudioPlayer>();
    }

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
        level = FindObjectOfType<Level>();
    }

    private void Update()
    {
        //CheckAndSetUnleashedReady();

        if (UnleashedReadyAndRequestedToBeActivated())
        {
            ActivateUnleashedAndStartTimer();
        }

        if (PlayerIsInUnleashedMode())
        {
            DeactiveUnleashedIfTimeIsUp();
        }
    }

    //private void CheckAndSetUnleashedReady()
    //{
    //    if (!gameState.GetUnleashedReady())
    //    {
    //        gameState.SetUnleashedReady(true);
    //        audioPlayer.PlayUnleashedReadySound();
    //    }
    //    else if (gameState.GetUnleashedReady())
    //    {
    //        gameState.SetUnleashedReady(false);
    //    }
    //}

    public void NotifyUnleashedReady()
    {
        audioPlayer.PlayUnleashedReadySound();
    }

    public void ActivateUnleashed()
    {
        FindObjectOfType<BackgroundMusic>().PlayUnleashedMusic();
        List<Ball> balls = FindPlayerBalls();
        startingBallRandomBounds = balls.ElementAt(0).GetRandomBounds();
        SetRandomBoundsOfAllPlayerBalls(balls, new Vector2(-10, 10));
        audioPlayer.PlayUnleashedActivatedSound();
        //gameState.ResetUnleashedScore();
        FindObjectOfType<Paddle>().gameObject.layer = LayerMask.NameToLayer("Default");
        gameState.SetUnleashedReady(false);
        gameState.SetIsUnleashed(true);
        gameState.SetAutoPlay(true);
        gameState.SetGameSpeed(4f);
    }

    public void DeactivateUnleashed(bool resetMusic = true)
    {
        List<Ball> balls = FindPlayerBalls();
        SetRandomBoundsOfAllPlayerBalls(balls, startingBallRandomBounds);
        FindObjectOfType<Paddle>().gameObject.layer = LayerMask.NameToLayer("Paddle");
        gameState.SetIsUnleashed(false);
        gameState.SetAutoPlay(false);
        LockAllPlayerBallsToPaddle();
        gameState.ResetGameSpeed();
        if (resetMusic)
        {
            PlayOriginalMusic();
        }
    }

    private void PlayOriginalMusic()
    {
        if (level.GetIsBossLevel())
        {
            FindObjectOfType<BackgroundMusic>().PlayBossMusic();
        }
        else
        {
            FindObjectOfType<BackgroundMusic>().PlayNormalMusic();
        }
    }

    private bool PlayerIsInUnleashedMode()
    {
        return gameState.GetIsUnleashed();
    }

    private void DeactiveUnleashedIfTimeIsUp()
    {
        if (timer.GetTimerCompleted())
        {
            DeactivateUnleashed();
        }
    }

    private void ActivateUnleashedAndStartTimer()
    {
        if (level.GetHasStarted())
        {
            ActivateUnleashed();
            timer.StartRealTimeTimer();
        }
    }

    private bool UnleashedReadyAndRequestedToBeActivated()
    {
        return gameState.GetUnleashedReady() && Input.GetKeyDown(KeyCode.Space);
    }

    private void SetRandomBoundsOfAllPlayerBalls(List<Ball> balls, Vector2 bounds)
    {
        for (int i = 0; i < balls.Count; i++)
        {
            balls.ElementAt(i).SetRandomBounds(bounds);
        }
    }

    private void LockAllPlayerBallsToPaddle()
    {
        Ball[] balls = FindObjectsOfType<Ball>();

        for (int i = 0; i < balls.Length; i++)
        {
            if (balls[i].tag == "PlayerBall")
            {
                balls[i].LockBallToPaddle();
            }
        }
    }

    private List<Ball> FindPlayerBalls()
    {
        Ball[] balls = FindObjectsOfType<Ball>();
        List<Ball> result = new List<Ball>();

        for (int i = 0; i < balls.Length; i++)
        {
            if (balls[i].tag == "PlayerBall")
            {
                result.Add(balls[i]);
            }
        }
        return result;
    }
}
