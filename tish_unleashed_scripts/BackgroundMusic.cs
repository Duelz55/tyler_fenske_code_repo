﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    [SerializeField] AudioClip startMusic;
    [Range(0, 1)] [SerializeField] float startMusicVolume;
    [SerializeField] AudioClip normalMusic;
    [Range(0, 1)] [SerializeField] float normalMusicVolume;
    [SerializeField] AudioClip bossMusic;
    [Range(0, 1)] [SerializeField] float bossMusicVolume;
    [SerializeField] AudioClip unleashedMusic;
    [Range(0, 1)] [SerializeField] float unleashedMusicVolume;
    [SerializeField] AudioClip winMusic;
    [Range(0, 1)] [SerializeField] float winMusicVolume;
    [SerializeField] AudioClip gameOverMusic;
    [Range(0, 1)] [SerializeField] float gameOverMusicVolume;

    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        audioSource.volume = startMusicVolume;    
    }

    public void PlayStartMusic()
    {
        PlaySong(startMusic, startMusicVolume);
    }

    public void PlayNormalMusic()
    {
        PlaySong(normalMusic, normalMusicVolume);
    }

    public void PlayBossMusic()
    {
        PlaySong(bossMusic, bossMusicVolume);
    }

    public void PlayUnleashedMusic()
    {
        PlaySong(unleashedMusic, unleashedMusicVolume);
    }

    public void PlayWinMusic()
    {
        PlaySong(winMusic, winMusicVolume);
    }

    public void PlayGameOverMusic()
    {
        PlaySong(gameOverMusic, gameOverMusicVolume);
    }

    private void PlaySong(AudioClip clip, float volume)
    {
        audioSource.Stop();
        audioSource.volume = volume;
        audioSource.clip = clip;
        audioSource.Play();
    }
}
