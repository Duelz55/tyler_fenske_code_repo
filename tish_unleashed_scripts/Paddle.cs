﻿using UnityEngine;


public class Paddle : MonoBehaviour
{
    [SerializeField] private float screenWidthInUnits;
    [SerializeField] GameObject loseSoulVFX;
    [SerializeField] GameObject shrinkVFX;
    [SerializeField] float leftWallPosition;
    [SerializeField] float rightWallPosition;

    const float SMALL_PADDLE_OFFSET = .625f;
    const float MED_PADDLE_OFFSET = 1.25f;
    const float LARGE_PADDLE_OFFSET = 2.5f;

    GameState gameState;
    AudioPlayer audioPlayer;
    VFXPlayer vfx;
    Level level;

    Vector2[] bounds;
    float[] paddleSize;
    int paddleIndex;
    float min;
    float max;

    private void Awake()
    {
        paddleIndex = 1;
        paddleSize = new float[] { .5f, 1, 2};
        bounds = new Vector2[] { new Vector2(SMALL_PADDLE_OFFSET + leftWallPosition, rightWallPosition - SMALL_PADDLE_OFFSET),
                                 new Vector2(MED_PADDLE_OFFSET + leftWallPosition, rightWallPosition - MED_PADDLE_OFFSET),
                                 new Vector2(LARGE_PADDLE_OFFSET + leftWallPosition, rightWallPosition - LARGE_PADDLE_OFFSET) };
    }

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
        audioPlayer = FindObjectOfType<AudioPlayer>();
        vfx = FindObjectOfType<VFXPlayer>();
        level = FindObjectOfType<Level>();
    }

    private void Update()
    {
        UpdatePaddlePosition();
        SetPaddleSize();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "EnemyBall")
        {
            if (gameState.GetPlayerSouls() >= 2 && !gameState.GetIsUnleashed() && level.GetHasStarted())
            {
                gameState.RemovePlayerSoul();
                vfx.TriggerVFX(loseSoulVFX, transform.position);
                audioPlayer.PlaySoulSound();
            }
        } else if (collision.gameObject.tag == "DeathBall")
        {
            if (!gameState.GetIsUnleashed() && level.GetHasStarted())
            {
                //audioPlayer.PlayDeathBallKillSound();
                level.SendToGameOver();
            }
        }
    }

    private void SetPaddleSize()
    {
        transform.localScale = new Vector3(paddleSize[paddleIndex], transform.localScale.y, transform.localScale.z);
        min = bounds[paddleIndex].x;
        max = bounds[paddleIndex].y;
    }

    private void UpdatePaddlePosition()
    {
        Vector2 paddlePos;
        if (gameState.IsAutoPlayEnabled())
        {
            paddlePos = getBallPosition();
        }
        else
        {
            paddlePos = getMousePosition();
        }

        transform.position = paddlePos;
    }

    private Vector2 getBallPosition()
    {
        return new Vector2(Mathf.Clamp(FindPlayerBall().transform.position.x, min, max), transform.position.y);
    }

    private Vector2 getMousePosition()
    {
        float mousePosInUnits = Input.mousePosition.x / Screen.width * screenWidthInUnits;
        Vector2 paddlePos = new Vector2(Mathf.Clamp(mousePosInUnits, min, max), transform.position.y);
        return paddlePos;
    }

    public void ResetPaddleSize()
    {
        paddleIndex = 1;
    }

    public void MakePaddleBigger()
    {
        paddleIndex = Mathf.Clamp(paddleIndex + 1, 0, 2);
    }

    public void MakePaddleSmaller()
    {
        int newIndex = Mathf.Clamp(paddleIndex - 1, 0, 2);
        if(newIndex != paddleIndex)
        {
            vfx.TriggerVFX(shrinkVFX, transform.position, 6);
            audioPlayer.PlayTableSizeSound();
            paddleIndex = newIndex;
        }
    }

    private Ball FindPlayerBall()
    {
        Ball[] balls = FindObjectsOfType<Ball>();

        for(int i = 0; i < balls.Length; i++)
        {
            if(balls[i].tag == "PlayerBall")
            {
                return balls[i];
            }
        }
        return null;
    }
}
