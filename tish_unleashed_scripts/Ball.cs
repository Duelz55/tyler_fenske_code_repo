﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] float constantSpeed;
    [SerializeField] Vector2 launchVelocityX;
    [SerializeField] float launchVelocityY;
    [SerializeField] float randomLowerBound = -0.2f;
    [SerializeField] float randomUpperBound = 0.2f;
    [SerializeField] Sprite normalSprite;
    [SerializeField] Sprite unleashedSprite;
    [SerializeField] bool isFinalEnemyBall = false;

    Vector2 distanceBetweenPaddleAndBall;
    AudioSource myAudioSource;
    Rigidbody2D myRigidBody2D;
    Level level;
    Paddle paddle;
    GameState gameState;

    bool isMoving = false;
    bool canFireBall = true;

    private void Awake()
    {
        myAudioSource = GetComponent<AudioSource>();
        myRigidBody2D = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
        level = FindObjectOfType<Level>();
        paddle = FindObjectOfType<Paddle>();
        distanceBetweenPaddleAndBall = transform.position - paddle.transform.position;
        IfPlayerBallAddToBallInventory();
    }

    private void IfPlayerBallAddToBallInventory()
    {
        if (tag == "PlayerBall")
        {
            level.AddBall();
        }
    }

    void Update()
    {
        if (tag == "PlayerBall")
        {
            HandlePlayerBall();
        }
        else if (tag == "EnemyBall")
        {
            HandleEnemyBall();
        }else if (tag == "DeathBall")
        {
            HandleDeathBall();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isMoving)
        {
            Vector2 velocityTweek = new Vector2(Random.Range(randomLowerBound, randomUpperBound), Random.Range(randomLowerBound, randomUpperBound));
            myRigidBody2D.velocity += velocityTweek;
            myAudioSource.Play();
        }
    }

    private void HandleDeathBall()
    {
        if (!isMoving)
        {
            LaunchDeathBall();
        }
        else
        {
            myRigidBody2D.velocity = constantSpeed * (myRigidBody2D.velocity.normalized);
        }
    }

    private void HandleEnemyBall()
    {
        if (!isMoving && !isFinalEnemyBall)
        {
            LaunchEnemyBallOnClick();
        }
        else if (isMoving)
        {
            myRigidBody2D.velocity = constantSpeed * (myRigidBody2D.velocity.normalized);
        }
    }

    private void HandlePlayerBall()
    {
        if (!isMoving)
        {
            LockBallToPaddle();
            LaunchBallOnClick();
            LaunchBallIfNotMovingDuringUnleashed();
        }
        else
        {
            myRigidBody2D.velocity = constantSpeed * (myRigidBody2D.velocity.normalized);
        }
    }

    private void LaunchBallIfNotMovingDuringUnleashed()
    {
        if (gameState.GetIsUnleashed())
        {
            LaunchBall();
        }
    }

    public void LockBallToPaddle()
    {
        isMoving = false;

        try
        {
            Vector2 paddlePos = new Vector2(paddle.transform.position.x, paddle.transform.position.y);
            transform.position = paddlePos + distanceBetweenPaddleAndBall;
        }catch (System.NullReferenceException)
        {
            try
            {
                Debug.Log(gameObject.name + "Couldn't Find Paddle, trying one more time");
                Paddle paddle = FindObjectOfType<Paddle>();
                distanceBetweenPaddleAndBall = transform.position - paddle.transform.position;

                Vector2 paddlePos = new Vector2(paddle.transform.position.x, paddle.transform.position.y);
                transform.position = paddlePos + distanceBetweenPaddleAndBall;
            }catch (System.NullReferenceException)
            {
                Debug.Log("Couldn't Find Paddle Object To Lock Ball To");
                FindObjectOfType<SceneLoader>().ReloadCurrentLevel();
            }
        }
    }

    private void LaunchBallOnClick()
    {
        if (Input.GetMouseButtonDown(0) && level.GetHasStarted())
        {
            LaunchBall();
        }
    }

    public void LaunchBall()
    {
        myRigidBody2D.velocity = new Vector2(Random.Range(launchVelocityX.x, launchVelocityX.y), launchVelocityY);
        isMoving = true;
    }

    private void LaunchEnemyBallOnClick()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            LaunchEnemyBall();
        }
    }

    private void LaunchEnemyBall()
    {
        isMoving = true;
        myRigidBody2D.velocity = new Vector2(Random.Range(launchVelocityX.x, launchVelocityX.y), launchVelocityY);
    }

    private void LaunchDeathBall()
    {
        isMoving = true;
        myRigidBody2D.velocity = new Vector2(Random.Range(launchVelocityX.x, launchVelocityX.y), launchVelocityY);
    }

    public bool GetIsMoving()
    {
        return isMoving;
    }

    public void SetRandomBounds(Vector2 bounds)
    {
        randomLowerBound = bounds.x;
        randomUpperBound = bounds.y;
    }

    public Vector2 GetRandomBounds()
    {
        return new Vector2(randomLowerBound, randomUpperBound);
    }
}
