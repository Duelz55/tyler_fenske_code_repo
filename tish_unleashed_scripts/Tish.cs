﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tish : MonoBehaviour
{
    [SerializeField] Sprite unleashedNotReady;
    [SerializeField] Sprite unleashedReady;
    [SerializeField] Sprite unleashed;
    [SerializeField] GameObject unleashedFX;

    GameState gameState;
    SpriteRenderer spriteRenderer;
    VFXPlayer vfx;

    bool unleashedIsReady = false;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        gameState = FindObjectOfType<GameState>();
        vfx = FindObjectOfType<VFXPlayer>();
    }

    private void Update()
    {
        UpdateTishSprite();
    }

    private void UpdateTishSprite()
    {
        if (!gameState.GetIsUnleashed())
        {
            SetSpriteToUnleashedReadyOrNotReadySprite();
        }
        else
        {
            spriteRenderer.sprite = unleashed;
        }
    }

    private void SetSpriteToUnleashedReadyOrNotReadySprite()
    {
        if (UnleashedIsReady())
        {
            unleashedIsReady = true;
            spriteRenderer.sprite = unleashedReady;
            vfx.TriggerVFX(unleashedFX, transform.position + new Vector3(.25f, 1, 0));
        }
        else if (UnleashedInNotReady())
        {
            unleashedIsReady = false;
            spriteRenderer.sprite = unleashedNotReady;
        }
    }

    private bool UnleashedInNotReady()
    {
        return !gameState.GetUnleashedReady() && unleashedIsReady;
    }

    private bool UnleashedIsReady()
    {
        return gameState.GetUnleashedReady() && !unleashedIsReady;
    }
}
