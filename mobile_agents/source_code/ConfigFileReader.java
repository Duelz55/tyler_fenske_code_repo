package MobileAgents;

import java.io.*;

/**
 * ConfigFileReader opens a file as an input stream, then passes the stream to
 * a buffered reader. This allows for adaptable file reading for when program
 * is transferred to a jar file. Configuration files will be read into the
 * program on execution in order to initialize the starting mote coordinates
 * and connections.
 * <p>
 * Config files contain line separated information about motes in the network.
 * Keywords are provided at the beginning of each line indicating what type of
 * information is being provided.
 * <p>
 * "node" indicates the following two non-space strings are the x and y
 * coordinates of a mote that needs to be added to the network.
 * <p>
 * "edge" indicates the following four non-space strings are the x and y
 * coordinates of two motes that need to be connected.
 * <p>
 * "fire" indicates a mote needs its MoteState changed to RED. The following
 * two non-space strings are the mote's x and y coordinates.
 * <p>
 * "station" indicates a mote needs to also be defined as the BaseStation.
 * The following two non-space strings are the mote's x and y coordinates.
 * <p>
 * created: 10/16/18 by Tyler H. Fenske (thf)
 * last modified: 11/09/18 by wdc, editing comments
 * previously modified: 10/16/18 by thf
 * @author Warren D. Craft
 * @author Tyler Fenske
 */
public class ConfigFileReader {

    //BufferedReader object allows reading over a file character by
    //character, or line by line.
    private BufferedReader file;
    private FileReader userFile;


    // ****************************** //
    //   Constructor(s)               //
    // ****************************** //

    /**
     * Constructor for a ConfigFileReader, requiring just the filename to be
     * read in, assuming the filename is the name of a File located in the
     * default resources folder within the src folder
     * @param fileName String name of the configuration file being used.
     */
    public ConfigFileReader(String fileName){

        openFileFromPath(fileName);

    }

    /**
     * Constructor for a ConfigFileReader, requiring an actual File object.
     * This is used in the project when files are being accessed outside
     * the default reources folder within the src folder.
     * @param file
     */
    public ConfigFileReader(File file){

        openFile(file);
    }

    /**
     * Opens a file as a stream, then passes that stream to a buffered reader
     * to allow for future string parsing.
     * @param fileName file being opened.
     */
    private void openFileFromPath(String fileName){
        ClassLoader cl = getClass().getClassLoader();
        InputStream in = cl.getResourceAsStream("resources/" +
                fileName);
        if(in != null){
            file = new BufferedReader(new InputStreamReader(in));

        }else{
            System.out.println("File not found");
        }
    }


    /**
     * Open a File object for reading, using FileReader and
     * BufferedReader objects.
     * @param file File object to be accessed/opened for reading
     */
    private void openFile(File file){
        try{
            userFile = new FileReader(file.getAbsolutePath());

            this.file = new BufferedReader(userFile);

        }catch(FileNotFoundException e){
            System.out.println("File not found!");
        }

    }

    /**
     * Returns whether or not the input stream was able to find a file with
     * the provided file name.
     * @return true if file was found, false otherwise
     */
    public boolean fileExists(){
        if(file == null){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Returns the next line of text from the file as a String.
     * @return next line of text, or a null string if at end of file.
     */
    public String getNextLine(){
        String result = null;

        try{
            result = file.readLine();
        }catch(Exception e){
            System.err.println(e.getMessage());
        }

        return result;
    }

    /**
     * Closes the bufferedReader stream. This should be done after all
     * data is read in from the file.
     */
    public void closeFileReader(){
        try{
            file.close();
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
}
