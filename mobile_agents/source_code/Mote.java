package MobileAgents;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.shape.Circle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Provides a structure to represent a remote sensing unit (Mote). Mote is
 * an extension of the Task class, allowing for thread application. Motes
 * have a state, and pass messages between their neighbors while still alive.
 *
 * A mote has 3 possible states:
 * BLUE - The mote is at least 2 motes away from any fires (Alive)
 * YELLOW - The mote has a neighbor that is on fire (Alive)
 * RED - The mote is on fire (Dead)
 *
 * While BLUE, a mote is considered alive, and will constantly check for new
 * messages. If a message is received that is addressed to the BaseStation,
 * it will pass the message to the neighbor with the lowest height — in the
 * direction of the BaseStation. If a mote receives the message "ON_FIRE" from
 * any of it's neighbors, the mote will turn YELLOW.
 *
 * When a mote first turns YELLOW, a MoteTimer is started. While YELLOW, the
 * mote will continue to process and pass messages along. When the timer ends,
 * the mote will turn RED.
 *
 * When a mote turns RED, it sends a dying message to all neighbors, then
 * ends its thread — by breaking out of the while loop in call()
 *
 * Motes have their own GUI objects (moteCircles and agentCircles) that they
 * use to change the JavaFX display when their MoteState is changed, or when
 * an agent is visiting / settling at that mote.
 *
 *
 * created: 10/16/18 by thf
 * last modified: 11/09/18 by thf - fixing up comments
 * previously modified: 10/21/18 by thf
 * @author Warren D. Craft
 * @author Tyler Fenske
 */
public class Mote extends Task<Void> {

    //inbox for messages passed by other motes
    private BlockingQueue<MoteMessage> inbox;
    //coordinate location of this particular mote
    private MoteLocation location;
    //unique ID number for this mote
    private int ID;
    //list of all motes that share an edge with this mote
    private ArrayList<Mote> neighbors;
    //color of this mote, BLUE, YELLOW, or RED
    private MoteState moteState;
    //True if a mobile agent is hovering over this mote
    private boolean mobileAgentPresent;
    //True if this mote is also the BaseStation
    private boolean isBaseStation = false;

    private boolean agentSettledHere = false;

    private boolean sentMessageToBaseStation = false;

    private String guestAgentID = "-1"; //-1 uninitialized agent id

    private MoteTimer timer;
    private final long FIRE_SPREAD_SPEED = 2000L;

    // providing reference(s) to the Mote's graphical representation.
    private Circle moteCircle;
    private Circle agentCircle;




    // ****************************** //
    //   Constructor(s)               //
    // ****************************** //

    /**
     * Constructor for a Mote object, which represents a remote
     * sensing unit (Mote)
     * @param location (x, y, z) tuple coordinate of this mote
     * @param moteState starting state of this mote
     */
    public Mote (MoteLocation location, MoteState moteState, int ID){
        this.location = location;
        this.moteState = moteState;
        this.ID = ID;

        inbox = new LinkedBlockingQueue<>();
        neighbors = new ArrayList<>();
    }


    // ****************************** //
    //   Overrides                    //
    // ****************************** //


    /**
     * This method is called any time a thread initialized with this task has
     * its start() method called. While the mote is non-RED, continuously
     * checks the blocking queue for incoming messages, and responds
     * accordingly. Continues to run until the mote turns RED — at which point
     * a dying message is sent to all neighbors, and this thread's execution
     * breaks out of the call method.
     */
    @Override
    protected Void call() throws Exception {

        while(moteState != MoteState.RED){

            // if(isCancelled) added by wdc Sun 11/04/18
            if ( isCancelled() ) {
                break;
            }

            //System.out.println("stuff");
            Thread.sleep(100);
            //This is the block of code that runs that is somehow causing
            //tremendous lag it seems

            readMessage();

            if(moteState == MoteState.YELLOW){
                if(timer.isTimerDone()){
                    setMoteState(MoteState.RED);
                }
            }

            if(agentSettledHere && !sentMessageToBaseStation){
                sendMessage(MoteMessage.MessageContent.NEW_AGENT);
                sentMessageToBaseStation = true;
            }

        }

        //Once the code breaks out of while loop, the mote has officially
        //died. Sending one final message to all neighbors to alert them
        //to its death.

        sendMessage(MoteMessage.MessageContent.ON_FIRE);

        //System.out.println(toString() + "Im dying");

        return null;
    }


    /**
     * Provides one of two necessary functions for establishing an equals()
     * comparison process for instances of the Mote class.
     * See the equals() method override as well.
     * @return int A hashCode for the Mote instance.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = hash + this.location.hashCode();
        hash = hash + this.getMoteState().hashCode();
        hash = hash + this.ID;
        return hash;
    }

    /**
     * A mote object is considered equal to another mote object if their
     * location in the network is exactly the same. In other words, their
     * x-coordinate, y-coordinate, and height are the same.
     * Might want to eventually generalize this to allow multiple Motes
     * at the same location but perhaps doing different things.
     * @param obj
     * @return true if equal, false otherwise
     */
    @Override
    public boolean equals(Object obj){
        if(obj.getClass() == this.getClass() ||
                obj.getClass() == BaseStation.class){
            Mote mote = (Mote) obj;
            return location.equals(mote.getLocation());
        }else{
            return false;
        }
    }

    /**
     * Override toString method for debugging.
     * @return String representation of Mote object.
     */
    @Override
    public String toString(){
        return "ID:" + ID + " | State:" + moteState + " | isBaseStation:" +
                isBaseStation + "\n" + location.toString();
    }


    // ****************************** //
    //       Utility Methods          //
    // ****************************** //

    /**
     * Adds a message to this mote's blocking queue. If the queue is full,
     * the thread will wait till it is no longer full to add a message.
     * @param message MoteMessage being added to the inbox
     */
    protected void receiveMessage(MoteMessage message){
        try{
            inbox.put(message);
        }catch(InterruptedException e){
            System.err.println(toString() + " " +e.getMessage());
        }

    }

    /**
     * Sends a message to the appropriate recipient based on the content
     * of the message created.
     * @param messageContent MessageContent contained in the message being sent
     */
    protected void sendMessage(MoteMessage.MessageContent messageContent) {
        MoteMessage moteMessage;

        switch(messageContent){
            case NEW_AGENT:
                moteMessage = new MoteMessage(
                        location, location, guestAgentID, messageContent);
                routeMessage(moteMessage);
                break;
            case ON_FIRE:
                moteMessage = new MoteMessage(
                        location, location, guestAgentID, messageContent);

                for(Mote mote : neighbors){
                    mote.receiveMessage(moteMessage);
                }
                break;

        }
    }

    /**
     * Naively routes messages towards the base station
     * by simply sending a message to the neighbor with the lowest height.
     * Resolves collisions by picking the first one found with the lowest
     * height.
     * @param message being sent to the base station
     */
    protected void routeMessage(MoteMessage message) {
        int height = Integer.MAX_VALUE;
        Mote moteRecipient = null;


        for(Mote mote : neighbors){
            //dont route messages back to/through the original sender
            if(!message.getSenderMoteLocation().equals(mote.getLocation())){
                if(height > mote.getLocation().getZ()){
                    height = mote.getLocation().getZ();
                    moteRecipient = mote;
                }
            }
        }

        if(moteRecipient != null){
            moteRecipient.receiveMessage(message);
        }

    }

    /**
     * Reads incoming messages, and determines how to handle them for
     * regular nodes (BaseStation has it's own way of handling).
     *
     */
    protected void readMessage(){
        try{
            MoteMessage nextMessage = inbox.peek();

            if(nextMessage != null){

                nextMessage = inbox.take();

                switch(nextMessage.getContent()){
                    //if message says ON_FIRE, this mote needs to turn yellow
                    //if it's blue, and remove the neighbor who caught fire
                    //from it's list of neighbors.
                    case ON_FIRE:
                        if(moteState.equals(MoteState.BLUE)){
                            setMoteState(MoteState.YELLOW);
                            //System.out.println(toString() + " turning yellow");
                        }

                        Mote moteToRemove = null;
                        for(Mote mote : neighbors){
                            if(mote.getLocation().equals
                                    (nextMessage.getSenderMoteLocation())){
                                moteToRemove = mote;
                            }
                        }
                        removeNeighbor(moteToRemove);



                        break;
                    case NEW_AGENT:
                        nextMessage.setRecentSender(location);
                        routeMessage(nextMessage);
                        break;
                }
            }

        }catch(InterruptedException e){
            System.err.println(e.getStackTrace() + "error in readMessage");
        }
    }


    // ****************************** //
    //   Other methods                //
    // ****************************** //

    /**
     * Adds a neighbor mote to this motes list of neighbors.
     * Motes are considered neighbors if they share an edge.
     */
    public void addNeighbor(Mote neighbor){
        if(!neighbors.contains(neighbor)){
            neighbors.add(neighbor);
        }
    }

    /**
     * Removes a neighbor mote to this motes list of neighbors.
     * Motes are considered neighbors if they share an edge.
     */
    protected synchronized void removeNeighbor(Mote neighbor){
        neighbors.remove(neighbor);
    }

    /**
     * Provides a way to obtain a mote's neighbor that doesn't already have
     * an agent (if such a mote exists). Not sure yet what to do if such a Mote
     * does NOT exist. The motivation for this method is to allow an agent to
     * "move" from one mote to a neighboring mote.
     * @return Mote which will be a neighbor of the current mote,
     *                          and that neighbor should not have an agent
     *                          associated with it
     */
    public synchronized Mote getRandomNeighborWithoutAgent () {

        if (neighbors.size() > 0) {
            // randomize the order of the neighbors
            Collections.shuffle(neighbors);
            // then take first neighbor mote that doesn't already have an agent
            // eventually this needs to be carefully synchronized!!!
            //doesn't return a random neighbor that's red either
            for ( Mote mote : neighbors ) {
                if ( !mote.isMobileAgentPresent() &&
                        mote.getMoteState() != MoteState.RED ) {
                    return mote;
                }
            }
            return null; // neighbors wasn't empty but each neighbor had an
            // agent already
        }

        return null;     // no neighbors found at all
    }

    /**
     * Initializes the timer associated with this mote, and starts it for
     * the time defined in FIRE_SPREAD_SPEED.
     */
    private void startMoteTimer(){
        timer = new MoteTimer(FIRE_SPREAD_SPEED);
        Thread thread = new Thread(timer);
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Checks the first message in the queue, without taking it out.
     * @return top MoteMessage in this mote's inbox BlockingQueue.
     */
    protected MoteMessage peekMessages(){
        return inbox.peek();
    }

    /**
     * Removes the top message of this mote's inbox BlockingQueue, then
     * returns that MoteMessage.
     * @return top MoteMessage in this mote's inbox BlockingQueue.
     */
    protected MoteMessage takeMessage(){
        try{
            return inbox.take();
        }catch(InterruptedException e){
            System.err.println(toString() + " " + e.getMessage());
        }

        return null;
    }


    /**
     * Prints list of connecting motes, prefacing with this mote's ID number.
     */
    public void printNeighbors(){
        if(!neighbors.isEmpty()){
            int count = 0;
            System.out.println("MOTE[" + ID + "] " + location.toString() +
                    " Neighbors:");
            for(Mote mote : neighbors){
                System.out.println("Neighbor(" + count + ") - " +
                        mote.getLocation().toString());
                count++;
            }
        }else{
            System.out.println("This mote has no neighbors and is very lonely");
        }
    }



    // ****************************** //
    //   Getter(s) & Setter(s)        //
    // ****************************** //

    /**
     * Returns the timer for this mote.
     * @return MoteTimer associated with this mote.
     */
    protected MoteTimer getTimer(){
        return timer;
    }

    /**
     * DisplaySetup calls this method to hand this mote its moteCircle
     * and agentCircle GUI objects. This allows the mote to alter its
     * circle's graphical representations from this thread indirectly through
     * the use of Platform.runlater() calls.
     */
    public void setMoteCircles(Circle moteCircle, Circle agentCircle) {
        this.moteCircle = moteCircle;
        this.agentCircle = agentCircle;
    }

    /**
     * Sets the MoteState of this mote to the specified color (BLUE, YELLOW,
     * or RED), and if the Mote has a reference to a graphical representation,
     * this sets the style for that graphical representation as well.
     */
    protected synchronized void setMoteState(MoteState moteState){
        this.moteState = moteState;

        // if graphical representation exists, set it to be updated
        // by the JavaFX graphics thread
        if ( moteCircle != null ) {

            switch ( moteState ) {

                case BLUE:
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            moteCircle.getStyleClass().clear();
                            moteCircle.getStyleClass().add("blue");
                        }
                    });
                    break;
                case YELLOW:
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            moteCircle.getStyleClass().clear();
                            moteCircle.getStyleClass().add("yellow");
                        }
                    });
                    break;
                case RED:
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            moteCircle.getStyleClass().clear();
                            moteCircle.getStyleClass().add("red");
                        }
                    });
                    break;

            } // end switch()

        } // end if()

        if(moteState.equals(MoteState.YELLOW)){
            startMoteTimer();
        }
    }



    /**
     * Sets the mobileAgentPresent boolean to true or false. Used by the
     * mobile agent to let the mote know if it is now hovering over or leaving
     * the mote.
     */
    public synchronized void setMobileAgentPresent(boolean mobileAgentPresent){
        if ( isMobileAgentPresent() != mobileAgentPresent ) {
            this.mobileAgentPresent = mobileAgentPresent;


            if(mobileAgentPresent){
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        agentCircle.getStyleClass().clear();
                        agentCircle.getStyleClass().add("visible");
                    }
                });
            }else{
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        agentCircle.getStyleClass().clear();
                        agentCircle.getStyleClass().add("invisible");
                    }
                });
            }
        }
    }

    /**
     * Sets the value of isBaseStation to the passed argument value.
     * @param isBaseStation true if this mote is a base station.
     */
    public void setIsBaseStation(boolean isBaseStation){
        this.isBaseStation = isBaseStation;
    }

    /**
     * Called by an agent that has decided to settle at this mote.
     */
    public void setAgentSettledHere(){
        agentSettledHere = true;
    }

    /**
     * Sets the string guest agent ID. When an agent is visiting or settles
     * on a mote, it will update this ID String.
     * @param guestAgentID String that provides a unique identifier to
     *                     visiting agent.
     */
    public void setGuestAgentID(String guestAgentID){
        this.guestAgentID = guestAgentID;
    }

    /**
     * Returns the agent ID of the visiting / settled agent.
     * @return agentID of visiting/settled agent.
     */
    protected String getAgentID(){
        return guestAgentID;
    }

    /**
     * Returns this mote's list of neighbors.
     * @return List of neighboring motes.
     */
    public ArrayList<Mote> getNeighbors(){
        return neighbors;
    }

    /**
     * Returns the MobileLocation object stored in this mote.
     * @return MoteLocation the 3-tuple (x, y, z) MoteLocation object.
     */
    public MoteLocation getLocation(){
        return location;
    }

    /**
     * Returns the MoteState enum value.
     * @return MoteState BLUE, YELLOW, RED -- See MoteState class for
     * more details
     */
    public synchronized MoteState getMoteState(){
        return moteState;
    }


    /**
     * Returns true if a mobile agent is hovering over this mote, and false
     * otherwise.
     * @return boolean mobileAgentPresent value
     */
    public synchronized boolean isMobileAgentPresent(){
        return mobileAgentPresent;
    }

    /**
     * Returns true if an agent has settled at this mote and false otherwise.
     * @return boolean agentSettledhere
     */
    public boolean isAgentSettledHere(){
        return agentSettledHere;
    }

}
