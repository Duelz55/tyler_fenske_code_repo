package MobileAgents;

import javafx.application.Application;
import javafx.stage.Stage;


/**
 * Provides the entry point for the Mobile_Agents project,
 * extending the JavaFX Application class and instantiating a
 * DisplaySetup class in the application's start() method.
 * @author Tyler Fenske
 * @author Warren D. Craft
 */
public class Main extends Application {

    // String filenames such as the one defined on the next line
    // will be used to search for files within the special resources folder
    // within the src folder. The file specified with this filename must
    // exist within the special resources folder within the src folder.
    private static String DEFAULT_CONFIGFILE_NAME = "configFile.txt";


    /**
     * The start() override method required of applications extending the
     * JavaFX Application class, serving to set up the initial display and
     * controls.
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{

        // for bare display with no initial network displayed
        // use the following DisplaySetup constructor and comment out
        // the 2nd constructor further below
        // DisplaySetup displaySetup =
        //    new DisplaySetup(primaryStage);

        // for augmented display with an initial network displayed
        // use the following contructor and comment out the
        // constructor appearing above
        DisplaySetup displaySetup =
            new DisplaySetup(primaryStage, DEFAULT_CONFIGFILE_NAME);

    }

    /**
     * The stop() override method required of applications extending the
     * JavaFX Application class; the stop() method is currently empty.
     */
    @Override
    public void stop(){
        // System.out.println("Goodbye!");
    }

    /**
     * The main() method within the Main class, serving to initiate the
     * required start() override method.
     * @param args
     */
    public static void main (String args[]){

        launch(args);

    }


}
