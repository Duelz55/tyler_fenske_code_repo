package MobileAgents;

import javafx.application.Platform;
import java.io.File;
import java.util.ArrayList;

/**
 * Provides a configuration of Motes, consisting of a network of Motes
 * and a single initial Agent, constructed from a Mote configuration file
 * taken as a (String) argument.
 *
 * Config files contain line separated information about motes in the network.
 * Keywords are provided at the beginning of each line indicating what type of
 * information is being provided.
 * <p>
 * "node" indicates the following two non-space strings are the x and y
 * coordinates of a mote that needs to be added to the network.
 * <p>
 * "edge" indicates the following four non-space strings are the x and y
 * coordinates of two motes that need to be connected.
 * <p>
 * "fire" indicates a mote needs its MoteState changed to RED. The following
 * two non-space strings are the mote's x and y coordinates.
 * <p>
 * "station" indicates a mote needs to also be defined as the BaseStation.
 * The following two non-space strings are the mote's x and y coordinates.
 *
 * created: 11/4/18 by Warren D. Craft (wdc)
 * last modified: 11/9/18 thf - cleaning up comments and print statements
 * previously modified: 11/4/18 by wdc
 * @author Warren D. Craft (wdc)
 * @author Tyler Fenske (thf)
 */
public class MoteConfiguration {

    private File configFile;
    private ArrayList<Mote> theMotes;
    private String configFileName;

    // ****************************** //
    //   Constructor(s)               //
    // ****************************** //

    /**
     * Constructor for a MoteConfiguration, which represents a network of Motes
     * along with a single initial Agent, constructed from a Mote configuration
     * file taken as a (String) argument.
     * @param configFileName String name of the configuration file
     */
    public MoteConfiguration(String configFileName) {
        this.configFileName = configFileName;
        initializeMotes(configFileName);
    }

    /**
     * Constructor for a MoteConfiguration, which represents a network of Motes
     * along with a single initial Agent, constructed from a Mote configuration
     * file taken as a (String) argument.
     * @param configFile File is a configuration file
     */
    public MoteConfiguration(File configFile) {
        this.configFile = configFile;
        //System.out.println("MoteConfiguration constructor: filename is " +
            //configFile.getName());
        // initializeMotes(configFile);
        initializeMotes(this.configFile);
    }

    // ****************************** //
    //   Getter(s) & Setter(s)        //
    // ****************************** //

    /**
     * Returns the ArrayList of Motes.
     * @return ArrayList<Mote>
     */
    public ArrayList<Mote> getTheMotes() {
        return theMotes;
    }



    // ****************************** //
    //   Private Utility Fxns         //
    // ****************************** //

    private void initializeMotes(String configurationFileName) {

        // creates a new file reader to read in the config file
        ConfigFileReader fr = new ConfigFileReader(configurationFileName);

        // stores the entire config file into an array list of strings.
        // Each string represents a line of text from the config file.
        ArrayList<String> configFileLines = new ArrayList<>();

        // If the file exists, loop over the config file until the end of file,
        // adding each line to the configFileLines array list as it goes.
        if(fr.fileExists()){
            String nextLine = fr.getNextLine();
            while(nextLine != null){
                configFileLines.add(nextLine);
                nextLine = fr.getNextLine();
            }

            // always should close any streams you're done with
            fr.closeFileReader();

            // Print out the config file to prove it worked
            //for(String line : configFileLines){
            //    System.out.println(line);
            //}

            // Initializes every mote based on the information provided in the
            // config file.
            theMotes = createMotes(configFileLines);

            // Print out the newly created motes to assure they
            // match the configFile
            //System.out.println("\n======================");
            //for(Mote mote : theMotes){
            //    System.out.println(mote);
            //}
            //System.out.println("======================");

            // Tells all motes who their neighbors are
            createConnections(configFileLines, theMotes);

            // Print out every mote and their neighbors
            //System.out.println("\n======================");
            //for(Mote mote : theMotes){
            //    mote.printNeighbors();
            //}
            //System.out.println("======================");

            //first index of motes array list is the baseStation.
            setMoteHeights((BaseStation) theMotes.get(0));

            // Print out every mote and their neighbors
            //System.out.println("\n======================");
            //for(Mote mote : theMotes){
            //   mote.printNeighbors();
            //}
            //System.out.println("======================");
            //System.out.println("Above output printed from within the " +
                //"MoteConfiguration class.");

        } else {

            System.out.println("No configuration file found, " +
                "terminating program.");
            Platform.exit();

        } // end if-else

    }

    /**
     * Given the configurationFile, parses each line into an arrayList. This
     * arrayList is then looped over, and each line is parsed into yet another
     * arrayList of Strings, separated by ' ' (the space character). Each
     * new arrayList of Strings is evaluated based on the first string of each
     * list. See class description for more details on this. Finally, all
     * appropriate motes are created and stored in a List.
     * @param configurationFile
     */
    private void initializeMotes(File configurationFile) {

        //System.out.println("Entering the initializeMotes() " +
        //       "method using a File.");
        //System.out.println("The filename is: " + configurationFile.getName());
        //System.out.println("Full path filename is: " +
        //        configurationFile.getAbsolutePath());

        // creates a new file reader to read in the config file
        ConfigFileReader fr = new ConfigFileReader(configurationFile);

        // stores the entire config file into an array list of strings.
        // Each string represents a line of text from the config file.
        ArrayList<String> configFileLines = new ArrayList<>();

        // If the file exists, loop over the config file until the end of file,
        // adding each line to the configFileLines array list as it goes.
        if(fr.fileExists()){
            //System.out.println("fr.fileExists() was true!");
            String nextLine = fr.getNextLine();
            //System.out.println("Next line was initially: " + nextLine);
            while(nextLine != null){
                configFileLines.add(nextLine);
                nextLine = fr.getNextLine();
            }

            // always should close any streams you're done with
            fr.closeFileReader();

            // Print out the config file to prove it worked
            //for(String line : configFileLines){
            //   System.out.println(line);
            // }

            // Initializes every mote based on the information provided in the
            // config file.
            theMotes = createMotes(configFileLines);

            // Print out the newly created motes to assure they
            // match the configFile
            //System.out.println("\n======================");
            //for(Mote mote : theMotes){
            //   System.out.println(mote);
            //}
            //System.out.println("======================");

            // Tells all motes who their neighbors are
            createConnections(configFileLines, theMotes);

            // Print out every mote and their neighbors
            //System.out.println("\n======================");
            //for(Mote mote : theMotes){
            //   mote.printNeighbors();
            //}
            //System.out.println("======================");

            //first index of motes array list is the baseStation.
            setMoteHeights((BaseStation) theMotes.get(0));

            // Print out every mote and their neighbors
            //System.out.println("\n======================");
            //for(Mote mote : theMotes){
            //   mote.printNeighbors();
            //}
            //System.out.println("======================");
            //System.out.println("Above output printed from within the " +
            //   "MoteConfiguration class.");

        } else {

            System.out.println("No configuration file found, " +
                "terminating program.");
            Platform.exit();

        } // end if-else

    }

    /**
     * Parses each string stored in the configFile array list (except for
     * strings that start with "edge" -- see createConnections()). Upon
     * parse, creates a mote object with x and y values provided by the
     * config file, and a temporary z value of -1 (the z value -- representing
     * height -- will be reassigned after all motes are created).
     * @param configFile list of strings read in from text file
     * @return ArrayList of motes that were created
     */
    private static ArrayList<Mote> createMotes (ArrayList<String> configFile){
        // List of all motes that are created. The first of which will always
        // be the base station [motes.get(0)].
        ArrayList<Mote> motes = new ArrayList<>();

        // This number is assigned to a new mote, then incremented by 1 for the
        // next new mote to be created. ID 0 will always be the base station.
        int IDNumber = 1;

        // First find the baseStation, and create it's mote (to avoid creating
        // it twice).
        for(String s : configFile){
            if(s.startsWith("station")){


                ArrayList<String> strings = new ArrayList<>();


                //x-coordinate starts at index 8
                //ArrayList is populated with x value, then y value

                String temp = "";
                for(int i = 8; i < s.length(); i++){

                    if(s.charAt(i) != ' '){
                        temp += s.charAt(i);
                    }else{
                        strings.add(temp);
                        temp = "";
                    }
                }
                strings.add(temp);

                //x value
                int x = Integer.parseInt(strings.get(0));
                //y value
                int y = Integer.parseInt(strings.get(1));
                //Height starts off unknown
                int z = -1;



                // BaseStation will always start in the blue state with an ID
                // number = 0
                BaseStation baseStation = new BaseStation(
                    new MoteLocation(x, y, z), MoteState.BLUE, 0);

                motes.add(baseStation);
            }else if(s.startsWith("fire")){
                ArrayList<String> strings = new ArrayList<>();


                //x-coordinate starts at index 5
                //ArrayList is populated with x value, then y value

                String temp = "";
                for(int i = 5; i < s.length(); i++){

                    if(s.charAt(i) != ' '){
                        temp += s.charAt(i);
                    }else{
                        strings.add(temp);
                        temp = "";
                    }
                }
                strings.add(temp);

                //x value
                int x = Integer.parseInt(strings.get(0));
                //y value
                int y = Integer.parseInt(strings.get(1));
                //Height starts off unknown
                int z = -1;

                // this mote has been specified to start on fire.
                Mote mote = new Mote(new MoteLocation(x, y, z),
                    MoteState.RED, IDNumber);

                IDNumber++;
                motes.add(mote);
            }
        }

        for(String s : configFile){
            if(s.startsWith("node")){
                ArrayList<String> strings = new ArrayList<>();


                //x-coordinate starts at index 5
                //ArrayList is populated with x value, then y value

                String temp = "";
                for(int i = 5; i < s.length(); i++){

                    if(s.charAt(i) != ' '){
                        temp += s.charAt(i);
                    }else{
                        strings.add(temp);
                        temp = "";
                    }
                }
                strings.add(temp);

                //x value
                int x = Integer.parseInt(strings.get(0));
                //y value
                int y = Integer.parseInt(strings.get(1));
                //Height starts off unknown
                int z = -1;

                // motes always start blue, but will eventually change color
                Mote mote = new Mote(new MoteLocation(x, y, z),
                    MoteState.BLUE, IDNumber);

                // if mote wasn't already created (the BaseStation and mote that
                // starts on fire is listed twice in the configFile format,
                // so this avoids creating two).
                if(!motes.contains(mote)){
                    IDNumber++;
                    motes.add(mote);
                }
            }
        }
        return motes;
    }


    /**
     * Parses remaining strings that weren't handled by createMotes(). Each
     * string that starts with "edge" provides two sets of coordinates
     * x1, y1 and x2, y2. These indicate that the two motes with these
     * coordinates are neighbors, and are therefore each added as
     * eachother's neighbor.
     * @param configFile list of strings read in from text file
     * @param motes list of all existing motes
     */
    private static void createConnections (ArrayList<String> configFile,
                                           ArrayList<Mote> motes){
        // Search the configFile for any entries starting with "edge"
        for(String s : configFile){
            if(s.startsWith("edge")){
                ArrayList<String> strings = new ArrayList<>();

                String temp = "";
                for(int i = 5; i < s.length(); i++){

                    if(s.charAt(i) != ' '){
                        temp += s.charAt(i);
                    }else{
                        strings.add(temp);
                        temp = "";
                    }
                }
                strings.add(temp);

                //x1 value
                int x1 = Integer.parseInt(strings.get(0));
                //y1 value
                int y1 = Integer.parseInt(strings.get(1));
                //x1 value
                int x2 = Integer.parseInt(strings.get(2));
                //y1 value
                int y2 = Integer.parseInt(strings.get(3));

                Mote mote1 = null;
                Mote mote2 = null;

                // Find the motes that need to be connected
                for(Mote mote : motes){
                    if(mote.getLocation().getX() == x1 &&
                        mote.getLocation().getY() == y1){
                        mote1 = mote;
                    }else if(mote.getLocation().getX() == x2 &&
                        mote.getLocation().getY() ==  y2){
                        mote2 = mote;
                    }
                }

                // If both motes were found to be existing motes
                // add them as neighbors
                if(mote1 != null && mote2 != null){
                    mote1.addNeighbor(mote2);
                    mote2.addNeighbor(mote1);
                }
            }
        }
    }

    /**
     * Set's the base station's height (the z dimension of it's x,y,z
     * MoteLocation) to 0 -- as this is the "base" of our structure.
     * Then call's setMoteHeightsRecursion() on each of it's neighbors.
     * @param baseStation the baseStation Mote
     */
    private static void setMoteHeights(BaseStation baseStation){
        // height for the base station is 0
        baseStation.getLocation().setZ(0);

        ArrayList<Mote> neighbors = baseStation.getNeighbors();

        for(Mote mote : neighbors){
            mote.getLocation().setZ(1);
        }

        for(Mote mote : neighbors){
            setMoteHeightsRecursion(mote);
        }
    }

    /**
     * Helper method for setMoteHeight(). This method is used to recursively
     * traverse through each mote's neighbor, and set their heights
     * accordingly. For each mote evaluated, each of their neighbor's
     * heights are checked.
     * If the neighbor has a height of -1 (their height hasn't been set up yet)
     * or their height is greater than the current mote's height + 1 (this
     * means that this path is shorter than the previously found path to
     * this mote), then the height of that neighbor is reassigned to the
     * current mote's height + 1.
     * @param m Mote being evaluated.
     */
    private static void setMoteHeightsRecursion(Mote m){
        ArrayList<Mote> neighbors = m.getNeighbors();
        //list of all Motes that were updated. These will be iterated over
        //recursively
        ArrayList<Mote> updated = new ArrayList<>();


        for(Mote mote : neighbors){
            if(mote.getLocation().getZ() == -1 ||
                mote.getLocation().getZ() > m.getLocation().getZ()+1){
                mote.getLocation().setZ(m.getLocation().getZ()+1);
                updated.add(mote);
            }
        }

        for(Mote mote : updated){
            setMoteHeightsRecursion(mote);
        }
    }


}
