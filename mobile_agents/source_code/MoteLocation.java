package MobileAgents;

import java.util.ArrayList;

/**
 * Provides a structure to represent the 3D location information of a
 * remote sensing unit (Mote) in integer coordinates.
 * created: 10/15/18 by Warren D. Craft (wdc)
 * last modified: 10/15/18 by wdc
 * @author Warren D. Craft
 * @author Tyler Fenske
 */
public class MoteLocation {

    private ArrayList<Integer> components = new ArrayList<>(3);
    // Not making the components FINAL, to leave open the possibility
    // of location modifications.

    // ****************************** //
    //   Constructor(s)               //
    // ****************************** //

    /**
     * Constructor for a MoteLocation object, which represents the 3D location
     * information of a remote sensing unit (Mote)
     * @param x int value of x location
     * @param y int value of y location
     * @param z int value of z location
     */
    public MoteLocation(int x, int y, int z) {
        components.add(x);
        components.add(y);
        components.add(z);
    }

    // ****************************** //
    //   Getter(s) & Setter(s)        //
    // ****************************** //

    /**
     * Returns the x-value or 1st-component of a 3D MoteLocation (x, y, z).
     * @return int 1st component of the (x, y, z) MoteLocation object.
     */
    public int getX() {
        return components.get(0);
    }

    /**
     * Returns the y-value or 2nd-component of a 3D MoteLocation (x, y, z).
     * @return int 2nd component of the (x, y, z) MoteLocation object.
     */
    public int getY() {
        return components.get(1);
    }

    /**
     * Returns the z-value or 3rd-component of a 3D MoteLocation (x, y, z).
     * @return int 3rd component of the (x, y, z) MoteLocation object.
     */
    public int getZ() {
        return components.get(2);
    }

    /**
     * Sets the z-value or 3rd-component of a 3D MoteLocation (x, y, z).
     * @param num indicating the height of the mote
     */
    public void setZ(int num) {
        components.set(2, num);
    }

    /**
     * Returns the x, y, z components of a (x, y, z) MoteLocation object, in
     * an ArrayList of ints.
     * @return ArrayList<Integer> the components of the (x, y, z)
     *                            MoteLocation object.
     */
    public ArrayList<Integer> getComponents() {
        return components;
    }

    // ****************************** //
    //   Override Fxns                //
    // ****************************** //

    /**
     * Provides a stringified version of a MoteLocation, in the form (x, y, z).
     * @return String of the form (x, y, z)
     */
    @Override
    public String toString() {
        int x = this.getX();
        int y = this.getY();
        int z = this.getZ();
        return "(" + x + ", " + y + ", " + z + ')';
    }

    /**
     * Provides one of two necessary functions for establishing a comparison
     * process for Tuples. See the .equals method override.
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 5;
        for (int component:components) {
            hash = 53 * hash + component;
        }
        return hash;
    }

    /**
     * Provides a method for checking equality of a MoteLocation to another
     * MoteLocation. This method is not intended to provide a way to naturally
     * order MoteLocations, but just define and check for equality.
     * Essentially: one MoteLocation is equal to another if and only if all
     * three corresponding component dimensions are equal.
     * MoteLocation(x, y, z) == MoteLocation(x', y', z')
     * iff x == x' && y == y' && z == z'
     */
    @Override
    public boolean equals(Object obj) {

        // if the object is compared with itself
        if ( obj == this ) {
            return true;
        }

        // check if obj is an instance of Tuple
        if ( ! (obj instanceof MoteLocation) ) {
            return false;
        }

        // for each component, check type and then details of component
        MoteLocation otherLocation = (MoteLocation) obj;
        ArrayList<Integer> otherComponents = otherLocation.getComponents();
        ArrayList<Integer> theseComponents = this.getComponents();
        for (int i = 0; i < otherComponents.size(); i++) {
            if ( !theseComponents.get(i).equals(otherComponents.get(i)) ) {

                return false;
            }
        }

        return true;

    }

}
