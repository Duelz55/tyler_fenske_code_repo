package MobileAgents;
import javafx.concurrent.Task;

/**
 * MoteTimer is a thread that exists for a specified amount of time. Once that
 * time is up, a flag is changed to indicate this. When motes turn yellow,
 * they create a MoteTimer to wait some amount of time before turning red.
 * created: 10/21/18 by thf
 * last modified: 11/09/18 by wdc
 * @author Warren D. Craft (wdc)
 * @author Tyler Fenske (thf)
 */
public class MoteTimer extends Task<Void> {

    private long time;
    private boolean timerDone = false;

    /**
     * Constructor for MoteTimer, taking as argument a (Long) specifying the
     * desired time interval in milliseconds.
     * @param time (Long) milliseconds of time
     */
    public MoteTimer(Long time){
        this.time = time;
    }

    /**
     * Provides the required call() override to extend the Task class,
     * using a sleep() method to let the desired time elapse then setting
     * its internal flag timerDone to true.
     * @return null
     * @throws Exception
     */
    @Override
    protected Void call() throws Exception {
        Thread.sleep(time);
        timerDone = true;
        return null;
    }

    /**
     * Allows caller to check if timing process has completed (i.e. if
     * timer's time has elapsed).
     * @return timerDone (boolean) indicating if desired time has elapsed
     */
    public boolean isTimerDone(){
        return timerDone;
    }
}
