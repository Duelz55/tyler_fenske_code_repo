package MobileAgents;

import javafx.application.Platform;
import javafx.scene.control.TextArea;
import java.util.ArrayList;
import java.util.List;

/**
 * Base Station provides a special Mote class for a Mote in a Mote network
 * that keeps track of mobile Agents in the Mote network. The Base Station
 * Mote is also special in having a reference to a JavaFX TextArea component
 * in the graphical representation of the Mote network, in which area the
 * Base Station Mote can supply information about the mobile Agents.
 *
 * created: 10/16/18 by thf
 * last modified: 11/09/18 by thf, commenting out print statements
 * previously modified: 11/09/18 by wdc, editing comments
 * @author Warren D. Craft (wdc)
 * @author Tyler Fenske (thf)
 */
public class BaseStation extends Mote {

    private List<MoteLocation> settledAgents;
    private List<String> settledAgentIDs;
    private TextArea outputTextArea;
    private boolean settledAgentAddedToList = false;

    // ****************************** //
    //   Constructor(s)               //
    // ****************************** //

    /**
     * Constructor for a BaseStation extension of a Mote, taking the same
     * information as the original Mote constructor.
     * @param location MoteLocation object specifying the location of the
     *                 BaseStation Mote
     * @param moteState MoteState enum of the form BLUE, YELLOW, or RED,
     *                  specifying the initial state of the BaseStation Mote
     * @param ID int an integer ID, with each Mote in a network having a unique
     *           such integer ID
     */
    public BaseStation(MoteLocation location, MoteState moteState, int ID){
        super(location, moteState, ID);
        super.setIsBaseStation(true);
        settledAgents = new ArrayList<>();
        settledAgentIDs = new ArrayList<>();
    }

    // ****************************** //
    //   Getter(s) & Setter(s)        //
    // ****************************** //

    /**
     * Allows the passing of a reference to an output TextArea of the GUI
     * to the BaseStation Mote so that the Base Station Mote can control
     * the String content of the GUI element
     * @param outputTextArea TextArea region in GUI for providing info
     *                       about network
     */
    public void setOutputTextArea(TextArea outputTextArea) {
        this.outputTextArea = outputTextArea;
    }


    // ****************************** //
    //   Method Overrides             //
    // ****************************** //

    /**
     * The required call() override method for the BaseStation Mote to
     * extend Task (as an extension of Mote). The call() method for the
     * BaseStation is similar to the call() method for a more general Mote,
     * but now also includes code to deal with messages about settled Agents
     * elsewhere in the network.
     * @return Void null as a call() method is want to do
     * @throws Exception
     */
    @Override
    protected Void call() throws Exception{

        while(getMoteState() != MoteState.RED){

            Thread.sleep(100);

            readMessage();

            if(getMoteState() == MoteState.YELLOW){
                if(getTimer().isTimerDone()){
                    setMoteState(MoteState.RED);
                }
            }

            if(isAgentSettledHere() && !settledAgentAddedToList){
                settledAgents.add(getLocation());
                settledAgentIDs.add(getAgentID());
                //printSettledAgents();
                updateOutputTextArea();
                settledAgentAddedToList = true;
            }

        }

        // Once the code breaks out of while loop, the mote has officially
        // died. Sending one final message to all neighbors to alert them
        // to its death.

        sendMessage(MoteMessage.MessageContent.ON_FIRE);

        // System.out.println("BASESTATION" + toString() + "Im dying");

        return null;
    }

    /**
     * Method for processing messages received, similar to the method used
     * in the more general Mote class, but now including specialized code to
     * process NEW_AGENT messages to keep track of new Agents and post
     * information in the GUI about new Agents.
     */
    @Override
    protected void readMessage(){
        MoteMessage nextMessage = peekMessages();

        if(nextMessage != null) {

            nextMessage = takeMessage();

            switch (nextMessage.getContent()) {
                //if message says ON_FIRE, this mote needs to turn yellow
                //if it's blue, and remove the neighbor who caught fire
                //from it's list of neighbors.
                case ON_FIRE:
                    if (getMoteState().equals(MoteState.BLUE)) {
                        setMoteState(MoteState.YELLOW);
                        //System.out.println(toString() + "" +
                            //"BaseStation turning yellow");
                    }

                    Mote moteToRemove = null;
                    for (Mote mote : getNeighbors()) {
                        if (mote.getLocation().equals
                            (nextMessage.getSenderMoteLocation())) {
                            moteToRemove = mote;
                        }
                    }
                    removeNeighbor(moteToRemove);

                    break;
                case NEW_AGENT:
                    settledAgents.add(nextMessage.getSenderMoteLocation());
                    settledAgentIDs.add(nextMessage.getAgentID());

                    // use the following to help check functionality during
                    // program modifications:
                    // printSettledAgents();

                    updateOutputTextArea();
                    break;
            }
        }
    }

    // ****************************** //
    //   Utility Fxns                 //
    // ****************************** //

    /**
     * A useful method when checking functionality
     * during program modifications.
     */
    private void printSettledAgents(){
        System.out.println("\n\n====================================\n\n");
        System.out.println("AGENTS SETTLED:");
        for(int i = 0; i < settledAgents.size(); i++){
            System.out.println(settledAgents.get(i).toString() + " Agent ID: " +
                settledAgentIDs.get(i));
        }
        System.out.println("\n\n====================================");
    }

    /**
     * Organizes and displays in the GUI the BaseStation's info about
     * settled agents. Note carefully the Platform.runLater() method used
     * to update JavaFX elements from outside the JavaFX thread.
     */
    private void updateOutputTextArea () {
        // construct the desired summary text
        String outputString = "SETTLED AGENTS:";
        for ( int i = 0; i < settledAgents.size(); i++ ) {
            outputString = outputString + "\n" +
                settledAgents.get(i).toString() +
                " Agent ID: " +
                settledAgentIDs.get(i);
        }
        final String outputStringToUse = outputString;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                outputTextArea.setText(outputStringToUse);
            }
        });

    }
}
