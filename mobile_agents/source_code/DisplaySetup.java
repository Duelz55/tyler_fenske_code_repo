package MobileAgents;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides an initial setup of the JavaFX graphics and GUI, with options for
 * both (a) including an initial network of Motes and Agent vs. (b) NOT
 * including an initial network of Motes and Agent. Setup includes
 * ComboBox for selecting a configFile and a Start button to initiate
 * network activity based on selected configFile.
 * created: 11/04/18 by wdc
 * last modified: 11/08/18 by wdc
 * @author Warren D. Craft (wdc)
 * @author Tyler Fenske (thf)
 */
public class DisplaySetup {

    private Stage      stage;
    private BorderPane borderPane;
    private List<Mote> theMotes;
    private List<Circle> moteCircles;
    private List<Circle> agentCircles;
    private File configFile;
    private String configFileName = "Select Config File";
    private TextArea outputTextArea;
    private Button btnStartStopReset;

    // helpful constants and other fields
    private final int PANE_WIDTH = 900;
    private final int PANE_HEIGHT = 600;
    private final int CENTER_DISPLAY_WIDTH = 600;
    private final int CENTER_DISPLAY_HEIGHT = 450;
    private final int CIRCLE_RADIUS = 10;
    private final int OUTER_CIRCLE_RADIUS = 15;
    private final int INSET = OUTER_CIRCLE_RADIUS;
    private double scale = 1.0;
    private String recentFileChoiceType = "default";

    // ****************************** //
    //   Constructor(s)               //
    // ****************************** //

    /**
     * Constructor for the initial display and GUI, readying the system for
     * LATER graphical representation of a network of Motes and
     * an initial Agent.
     * @param stage An instance of a JavaFX Stage
     */
    public DisplaySetup(Stage stage) {
        this.stage = stage;
        // perhaps eliminate the Circles allocation as well?
        this.moteCircles = new ArrayList<>();
        this.agentCircles = new ArrayList<>();

        setupBasic();
    }

    /**
     * Constructor for the initial display and GUI, which also includes an
     * initial network to be displayed.
     * @param stage An instance of a JavaFX Stage
     * @param initialConfigFileName String giving the name of the initial
     *                              configuration file, assumed to be located
     *                              in the resource folder inside the src
     *                              folder
     */
    public DisplaySetup(Stage stage, String initialConfigFileName) {

        this.stage = stage;
        this.moteCircles = new ArrayList<>();
        this.agentCircles = new ArrayList<>();
        configFileName = initialConfigFileName;
        setupBasic();

        // convert the initial config file name into an actual file
        // note carefully the assumed relative path of the initial config file
        configFile =
            new File(ClassLoader.getSystemResource(
                "resources/configFile.txt").getFile()
            );

        // establish the MoteConfiguration from the initial congfig file
        MoteConfiguration moteConfiguration =
            new MoteConfiguration(configFileName);

        // display the associated graphics in the window
        displayMoteConfiguration(moteConfiguration);

        // allow Start button to be active
        btnStartStopReset.setDisable(false);

    }

    // ****************************** //
    //   Getter(s) & Setter(s)        //
    // ****************************** //

    // ****************************** //
    //   Public Methods               //
    // ****************************** //

    // ****************************** //
    //   Utility Fxns                 //
    // ****************************** //

    /**
     * Set up the basic GUI, leaving space in the BorderPane's Center
     * for later display of a network configuration
     */
    private void setupBasic () {

        //======================================//
        // Organize the Scene & Stage           //
        //======================================//

        // A BorderPane to hold everything
        borderPane = new BorderPane();

        // An HBox to eventually hold some buttons/controls
        // in the top portion of the BorderPane
        HBox topControlsHBox = new HBox();
        topControlsHBox.getStyleClass().add("hbox");

        // An HBox to eventually hold some buttons/controls
        // in the bottom portion of the BorderPane
        HBox bottomControlsHBox = new HBox();
        bottomControlsHBox.getStyleClass().add("hbox");

        // A Box to hold some stuff in the Left side of the BorderPane
        VBox outputVBox = new VBox();
        outputVBox.getStyleClass().add("vbox");
        outputVBox.setPrefWidth(250);
        outputVBox.setPadding(
            new Insets(10, 10, 10, 10)
        );
        outputVBox.setSpacing(10);
        outputVBox.setAlignment(Pos.TOP_CENTER);
        Label configFileLabel =
            new Label("Using Configuration from:");
        configFileLabel.getStyleClass().add("configFileLabel");
        configFileLabel.setPrefWidth(240);
        Label configFileNameLabel =
            new Label(configFileName);
        configFileNameLabel.getStyleClass().add("fileName");
        configFileNameLabel.setPrefWidth(240);
        //  a spacing region
        Region spacingRegion = new Region();
        spacingRegion.setPrefHeight(50);
        Label outputLabel = new Label("UPDATES");
        outputLabel.getStyleClass().add("outputLabel");
        outputTextArea =
            new TextArea("Settled Agents Posted Here.");
        outputTextArea.getStyleClass().add("outputTextArea");
        outputTextArea.setEditable(false);

        // Some possibly useful buttons/controls
        Button btn01 = new Button("Bigger Circles");
        btn01.setPrefWidth(125);
        Button btn02 = new Button("Smaller Circles");
        btn02.setPrefWidth(125);
        btnStartStopReset = new Button("Start");
        btnStartStopReset.setPrefWidth(125);
        btnStartStopReset.setDisable(true); // disabled until file chosen

        ComboBox<String> selectionBox = new ComboBox<>();
        selectionBox.setValue("Select Config File");
        selectionBox.getItems().addAll(
            "configFile.txt",
            "configFileGrid05x05.txt",
            "configFileGrid05x05WithDiags.txt",
            "configFileGrid10x10.txt",
            "configFileGrid10x10WithDiags.txt",
            "configFileGrid20x20.txt",
            "configFileGrid20x20WithDiags.txt",
            "Select ..."
        );

        setButtonHandlers(btn01);
        setButtonHandlers(btn02);
        setButtonHandlers(btnStartStopReset);
        setComboBoxHandler(selectionBox,
                           configFileNameLabel,
                           btnStartStopReset);

        topControlsHBox.getChildren().addAll(selectionBox, btnStartStopReset);
        borderPane.setTop(topControlsHBox);
        bottomControlsHBox.getChildren().addAll(btn01, btn02);
        borderPane.setBottom(bottomControlsHBox);
        outputVBox.getChildren().addAll(
            configFileLabel, configFileNameLabel, spacingRegion,
            outputLabel, outputTextArea
        );
        borderPane.setLeft(outputVBox);

        // create the scene
        Scene scene = new Scene(borderPane, PANE_WIDTH, PANE_HEIGHT);

        // frustrating rigamarole to use the styles sheet
        URL url = this.getClass().getResource("styles.css");
        if (url == null) {
            System.out.println("CSS resource not found!");
            System.exit(-1);
        }
        String theCSS = url.toExternalForm();
        scene.getStylesheets().add(theCSS);

        stage.setTitle("Mobile Agents");

        // add scene to the stage
        stage.setScene(scene);

        // display contents of the stage
        stage.show();

    }


    /**
     * Sets up the EventHandlers for various buttons, use a switch() on the
     * original button text string
     * @param button
     */
    private void setButtonHandlers (Button button) {

        String buttonText = button.getText();

        switch ( buttonText ) {

            case "Bigger Circles":
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        Double rMote = moteCircles.get(0).getRadius();
                        Double newRMote = rMote + 1;
                        Double rAgent = agentCircles.get(0).getRadius();
                        Double newRAgent = rAgent + 1;
                        for (Circle circle : moteCircles) {
                            circle.setRadius(newRMote);
                        }
                        for (Circle circle : agentCircles) {
                            circle.setRadius(newRAgent);
                        }
                    }
                });
                break;

            case "Smaller Circles":
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        Double rMote = moteCircles.get(0).getRadius();
                        Double newRMote = rMote - 1;
                        Double rAgent = agentCircles.get(0).getRadius();
                        Double newRAgent = rAgent - 1;
                        for (Circle circle : moteCircles) {
                            circle.setRadius(newRMote);
                        }
                        for (Circle circle : agentCircles) {
                            circle.setRadius(newRAgent);
                        }
                    }
                });
                break;

            case "Start":
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if ( button.getText().equals("Start") ) {
                            button.setText("Reset");
                            if ( configFile != null ) {

                                for(Mote mote : theMotes){
                                    Thread thread = new Thread(mote);
                                    thread.setDaemon(true);
                                    thread.start();
                                }

                                Agent agent =
                                    new Agent(theMotes.get(0), true);
                                Thread thread = new Thread(agent);
                                thread.setDaemon(true);
                                thread.start();
                            }
                        } else if ( button.getText().equals("Reset") ) {
                            button.setText("Start");
                            // redraw the graphical representation of the
                            // most recently-selected MoteConfiguration
                            MoteConfiguration moteConfiguration;
                            if ( recentFileChoiceType.equals("default")) {
                                // use the String configFileName
                                // because most recent config was from
                                // drop-down list
                                moteConfiguration =
                                    new MoteConfiguration(configFileName);
                            } else {
                                // use the configFile
                                // because most recent config was obtained from
                                // file chooser
                                moteConfiguration =
                                    new MoteConfiguration(configFile);
                            }

                            // display the associated graphics in the window
                            displayMoteConfiguration(moteConfiguration);

                            // clear output area of possibly outdated
                            // Agent info
                            outputTextArea.setText(
                                "Settled Agents Posted Here"
                            );
                        }
                    }
                });
                break;

            default:
                break;

        } // end switch()

    } // end setButtonHandlers()

    /**
     * Sets up the EventHandler for the ComboBox, including the FileChooser
     * in response to the “Select …” option.
     * @param  comboBox the instantiated ComboBox
     * @param label JavaFX label used for displaying choice of config file
     * @param theButton JavaFX button whose functionality can be enabled or
     *                  disabled based on comboBox choice(s)
     */
    private void setComboBoxHandler (ComboBox<String> comboBox,
                                     Label label,
                                     Button theButton) {

        comboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Desktop desktop = Desktop.getDesktop();
                FileChooser fileChooser = new FileChooser();
                String theSelection = comboBox.getValue();
                if ( theSelection.equals("Select ...") ) {

                    // save possible File safely without potentially corrupting
                    // configFile with a null
                    File selectedFile = fileChooser.showOpenDialog(stage);

                    if ( selectedFile != null ) {
                        configFile = selectedFile;
                        String fileName = configFile.getName();
                        String fileNameWithPath = configFile.getPath();

                        label.setText( fileName );

                        // establish the MoteConfiguration to be used using
                        // an actual File reference instead of name & path:
                        MoteConfiguration moteConfiguration =
                            new MoteConfiguration(configFile);

                        // display the associated graphics in the window?
                        displayMoteConfiguration(moteConfiguration);

                        // clear the outputTextArea of possibly old Agent info
                        outputTextArea.setText(
                            "Settled Agents Posted Here"
                        );

                        theButton.setText("Start");
                        theButton.setDisable(false);
                        recentFileChoiceType = "nondefault";

                    } else {
                        // user probably hit "cancel" instead of choosing
                        // a file, so leave configFile and associated filename
                        // alone
                    }

                    // try to clear the selection so that "Select ..." can
                    // be re-selected without an intervening selection ...
                    // something like the following was recommended in various
                    // forums, but it causes other problems. The line is left
                    // here commented out for future development. Later might
                    // be simplest to implement the file chooser through a
                    // separate button.
                    // comboBox.valueProperty().set(null);

                } else {

                    label.setText(theSelection);

                    // set the flag telling us that recent file choice
                    // was from the default list
                    // so that later re-sets will realize we need to use
                    // simple filename and not the file itself

                    recentFileChoiceType = "default";
                    // perhaps set configFile to null?

                    // use the predetermined file to build configuration

                    String fileName = theSelection;
                    configFileName = fileName;

                    // Generate the MoteConfiguration
                    // using the String fileName, which will only work
                    // for files in the correct "default" location
                        MoteConfiguration moteConfiguration =
                            new MoteConfiguration(fileName);

                    // then display the associated graphics in
                    displayMoteConfiguration(moteConfiguration);

                    // clear the outputTextArea of possibly old Agent info
                    outputTextArea.setText(
                        "Settled Agents Posted Here"
                    );

                    theButton.setText("Start");
                    theButton.setDisable(false);

                }


            }
        });

    }

    /**
     * Constructs the graphical representation (circles, lines, etc) of
     * a mote network and places the graphics within the Center of the
     * BorderPane.
     * @param moteConfiguration an instance of the MoteConfiguration class.
     */
    private void displayMoteConfiguration (MoteConfiguration moteConfiguration)
    {

        // use the ArrayList<Mote> in the moteConfiguration to set up the
        // graphics in the usual way and display them, replacing any graphics
        // that might already appear in the CENTER of the BorderPane

        theMotes = moteConfiguration.getTheMotes();
        moteCircles.clear();
        agentCircles.clear();

        //======================================//
        // Generate the Graphic Elements        //
        // Corresponding to the Motes & Agent   //
        //======================================//

        // (1) Determine an appropriate scale so the Mote network will
        //     use a good portion of the display area
        int XMax = 0;
        int YMax = 0;
        for ( Mote mote : theMotes ) {
            // grab the Mote's location information
            MoteLocation moteLoc = mote.getLocation();
            // update 2D maxima
            XMax = Math.max(XMax, moteLoc.getX());
            YMax = Math.max(YMax, moteLoc.getY());
        }
        scale = Math.min(  (double)(CENTER_DISPLAY_HEIGHT - 2 * INSET)/YMax,
            (double)(CENTER_DISPLAY_WIDTH - 2 * INSET)/XMax   );
        // System.out.println("The scale is: " + scale);

        // (2) create the Circles corresponding to Motes and (potential)
        //     Agents; use the scale from above to set the corresponding
        //     Circle locations; then each time hand references to the
        //     associated Mote.

        for ( Mote mote : theMotes ) {

            // grab the Mote's location information
            MoteLocation moteLoc = mote.getLocation();

            // create and mote and agent circles at the (scaled) location
            Circle moteCircle = new Circle( scale * moteLoc.getX(),
                scale * moteLoc.getY(),
                CIRCLE_RADIUS );
            Circle agentCircle = new Circle( scale * moteLoc.getX(),
                scale * moteLoc.getY(),
                OUTER_CIRCLE_RADIUS );
            // set style properties for the mote Circle
            switch ( mote.getMoteState() ) {
                case BLUE:
                    moteCircle.getStyleClass().add("blue");
                    break;
                case YELLOW:
                    moteCircle.getStyleClass().add("yellow");
                    break;
                case RED:
                    moteCircle.getStyleClass().add("red");
                    break;
            }
            // set style properties for the (potential) agent Circle
            if ( mote.isMobileAgentPresent() ) {
                agentCircle.getStyleClass().add("visible");
            } else {
                agentCircle.getStyleClass().add("invisible");
            }

            // add the circles to the lists
            moteCircles.add( moteCircle );
            agentCircles.add( agentCircle );

            // hand Circle refs to the associated Mote
            mote.setMoteCircles(moteCircle, agentCircle);

        }

        // assuming base station mote is first in list,
        // modify its style
        moteCircles.get(0).getStyleClass().remove("blue");
        moteCircles.get(0).getStyleClass().add("basestation");

        // hand the base station a ref to the display's output TextArea
        BaseStation theBaseStation = (BaseStation)theMotes.get(0);
        theBaseStation.setOutputTextArea(outputTextArea);

        // (3) Create the graphical lines representing neighbor connections.
        //     This uses the MoveTo, LineTo, and Path classes

        Path thePath = new Path();
        List<Mote> processedMotes = new ArrayList<>();
        for ( Mote mote :  theMotes ) {
            // get mote location information
            MoteLocation tempLoc = mote.getLocation();
            // get mote's neighbors
            List<Mote> neighbors = mote.getNeighbors();

            // for all Motes in the neighbor list
            for (Mote neighbor : neighbors) {

                if (! (processedMotes.indexOf(neighbor) >= 0) ) {
                    // then neighbor wasn't previous processed so ...
                    MoteLocation neighborLoc = neighbor.getLocation();
                    MoveTo tempMoveTo =
                        new MoveTo(scale * tempLoc.getX(),
                            scale * tempLoc.getY());
                    LineTo tempLineTo =
                        new LineTo(scale * neighborLoc.getX(),
                            scale * neighborLoc.getY());
                    // add elements to the path
                    thePath.getElements().addAll(tempMoveTo, tempLineTo);
                }
            }

            // set Mote as having been processed
            processedMotes.add(mote);

        }
        thePath.setStrokeWidth(2);

        // Group all the Circle and Line graphical elements to eventually
        // put them in the Center of the BorderPane
        Group root = new Group ();
        root.getChildren().add(thePath);
        for ( int i = 0; i < moteCircles.size(); i++) {
            root.getChildren().add(moteCircles.get(i));
            root.getChildren().add(agentCircles.get(i));
        }

        borderPane.setCenter(root);
    }

}
