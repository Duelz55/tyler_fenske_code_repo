package MobileAgents;

/**
 * Provides the structure for messages passed among Motes, with
 * structural details still being developed. Currently a MoteMessage really
 * only has 2 types:
 * (1) a message intended only for a mote's neighbors, which will signal that
 *     the sender mote is on fire;
 * (2) a message intended for the Base Station mote, which will be an update
 *     with information about a mobile agent's ID and location.
 * created: 10/16/18 by wdc
 * last modified: 10/16/18 by wdc
 * @author Warren D. Craft
 * @author Tyler Fenske
 */
public class MoteMessage {

    // all of these fields can be final -- shouldn't change once set
    private final MoteLocation ORGINAL_SENDER;
    private final String AGENT_ID;
    private final MessageContent CONTENT;
    private MoteLocation recentSender;
    public enum MessageContent {NEW_AGENT, ON_FIRE}

    // ****************************** //
    //   Constructor(s)               //
    // ****************************** //

    /**
     * Simplest MoteMessage constructor, requiring only the sender and
     * MessageContent to be specified.
     * @param originalSender MoteLocation indicating location of Mote
     *                       originating the MoteMessage
     * @param content MessageContent enum either NEW_AGENT or ON_FIRE
     */
    public MoteMessage(MoteLocation originalSender, MessageContent content) {

        // used when sending "FIRE" alert to neighbors;
        // only the TARGET info is needed
        // this might change in more general circumstances later
        this (originalSender, null, "-1", content);
    }

    /**
     * General constructor, requiring all info to be specified.
     * @param originalSender MoteLocation object representing the location
     *                           of the mote sender of the message
     * @param agentID int value representing associated mobile agent ID
     * @param content  MessageContent an enum being NEW_AGENT or ON_FIRE
     */
    public MoteMessage(MoteLocation originalSender,
                       MoteLocation recentSender,
                       String agentID,
                       MessageContent content) {

        // usually used when sending info to Base Station mote, updating
        // info about an ID and location of an associated mobile agent
        this.ORGINAL_SENDER = originalSender;
        this.AGENT_ID = agentID;
        this.CONTENT = content;
        this.recentSender = recentSender;
    }

    // ****************************** //
    //   Getter(s) & Setter(s)        //
    // ****************************** //

    /**
     * Returns the location (in the form of a MoteLocation object) of
     * the originator of the message.
     * @return MoteLocation the location of the original message sender Mote
     */
    public MoteLocation getSenderMoteLocation() {
        return ORGINAL_SENDER;
    }

    /**
     * Returns the ID of the Agent associated with the message-originating
     * Mote. For a ON_FIRE message content type, the Agent ID might be -1,
     * indicating no Agent present.
     * @return String ID of Agent associated with message-originating Mote
     */
    public String getAgentID() {
        return AGENT_ID;
    }

    /**
     * Returns the MessageContent of a MoteMessage, in the form of an enum
     * NEW_AGENT or ON_FIRE, indicating the type of message. NEW_AGENT
     * messages are sent in response to an Agent "settling" on a Mote, with
     * the Mote Base Station the intended recipient. ON_FIRE messages are
     * sent by a Mote when it "catches fire" (its MoteState turns RED),
     * with intended recipients being all neighbors of the Mote that has
     * caught fire.
     * @return MessageContent an enum being NEW_AGENT or ON_FIRE
     */
    public MessageContent getContent() {
        return CONTENT;
    }

    /**
     * Provides the location in the form of a MoteLocation object of the
     * Mote that most recently handled a MoteMessage.
     * @param moteLocation MoteLocation being the location of the Mote that
     *                     most recently "handled" (sent, received,
     *                     forwarded) the MoteMessage. Initially, the original
     *                     sender and recent sender of a MoteMessage are the
     *                     same.
     */
    public void setRecentSender(MoteLocation moteLocation){
        recentSender = moteLocation;
    }

    // ****************************** //
    //   Override Fxns                //
    // ****************************** //

    /**
     * Provides a string version of the MoteMessage.
     * @return String a string version of the MoteMessage
     */
    @Override
    public String toString(){
        return getContent().toString();
    }


}
