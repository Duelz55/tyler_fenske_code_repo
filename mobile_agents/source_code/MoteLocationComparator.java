

package MobileAgents;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Provides a comparator for MoteLocation objects, ordering such objects
 * in a standard alphanumeric way for vectors or 3D coordinate points, where
 * (x, y, z) < (x', y', z')
 * if x < x', OR if x = x' and y < y', OR if x = x' and y = y' and z < z'
 * created: 10/15/18 by Warren D. Craft (wdc)
 * last modified: 10/15/18 by wdc
 * @author Warren D. Craft
 * @author Tyler Fenske
 */
public class MoteLocationComparator implements Comparator<MoteLocation> {

    /**
     * Provides the primary comparison process for MoteLocations
     * @param mloc01 1st instance of a MoteLocation
     * @param mloc02 2nd instance of a MoteLocation
     * @return
     */
    @Override
    public int compare(MoteLocation mloc01, MoteLocation mloc02) {
        ArrayList<Integer> components01 = mloc01.getComponents();
        ArrayList<Integer> components02 = mloc02.getComponents();
        if ( components01.get(0) < components02.get(0) ) {
            return -1; // leave order as is
        } else if ( components02.get(0) < components01.get(0) ) {
            return 1;  // reverse order
        } else {       // x components are equal, so check y components

            if ( components01.get(1) < components02.get(1) ) {
                return -1; // leave order as is
            } else if ( components02.get(1) < components01.get(1) ) {
                return 1;  // reverse order
            } else {   // y components are equal, so check z components

                if ( components01.get(2) < components02.get(2) ) {
                    return -1; // leave order as is
                } else if ( components02.get(2) < components01.get(2) ) {
                    return 1;  // reverse order
                }
            }

        }
        return 0; // corresponding coordinates are equal
    }
}
