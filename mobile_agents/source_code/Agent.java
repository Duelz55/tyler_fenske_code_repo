package MobileAgents;

import javafx.concurrent.Task;

/**
 * Mobile agents have two phases.
 *
 * Phase 1: Starting at the base station, a single agent is created to randomly
 * walk between mote neighbors until it has found a yellow mote.
 * Phase 2: Once a yellow mote has been found, the agent "settles" on that
 * mote, letting the mote know it is there to stay. The agent then creates
 * "cloned agents" to all neighboring motes that don't already contain an
 * agent and aren't already on fire.
 *
 * The mote object itself will handle all GUI elements associated with the
 * agent's location.
 *
 * The first agent created starts with an agent ID of "-1-1-1". Once it finds
 * the mote it will settle on (the beginning of phase 2), it will change its
 * ID to a concatenation of the x + y + z coordinates of that mote. All cloned
 * motes will adopt this agent ID format of the x + y + z coordinates.
 *
 * created: 10/20/18 by thf
 * last modified: 11/09/18 by wdc, editing comments
 * previously modified: 10/20/18 by thf
 * @author Warren D. Craft (wdc)
 * @author Tyer Fenske (thf)
 */
public class Agent extends Task<Void> {

    private Mote hostMote;
    private boolean isPhaseOne;
    private boolean firstStep = true;
    private String agentID;
    private MoteTimer timer;
    private final long AGENT_WALK_SPEED = 50L;

    // ****************************** //
    //   Constructor(s)               //
    // ****************************** //

    /**
     * Constructor for an Agent, specifying the Agent's location in terms
     * of the Mote it "sits" on and whether the network/agent is in Phase 1
     * (i.e. the Agent is randomly walking among the Motes in the network) or
     * Phase 2 (the Agent stays on whatever Mote it is assigned to).
     * @param hostMote Mote the Mote the Agent is currently "sitting" on
     * @param isPhaseOne boolean whether Agent is in its random walk phase
     *                   (Phase 1) or not
     */
    public Agent(Mote hostMote, boolean isPhaseOne){
        this.hostMote = hostMote;
        this.isPhaseOne = isPhaseOne;
        hostMote.setMobileAgentPresent(true);
        agentID = setAgentID();
        hostMote.setGuestAgentID(agentID);
    }

    /**
     * The required override call() method to extend Task. The Agent will
     * randomly walk during phase one until a yellow mote is found.
     * After phase one is over, alerts the mote that it is settling here,
     * and clones to neighbors.
     * @return Void
     */
    @Override
    protected Void call() {
        try{
            //While it's phase one, agent will randomly walk until it finds a
            //Yellow mote.
            while(isPhaseOne){

               if(firstStep){
                   initializeTimer();
                   firstStep = false;
               }

               if(!timer.isTimerDone()){

                   //The agent found a yellow mote, so phase1 is over

                   //Threads are lazy and if you don't give them random 100ms
                   //breaks, they refuse to work!!!!!
                   Thread.sleep(100);
                   if(hostMote.getMoteState().equals(MoteState.YELLOW)){
                       isPhaseOne = false;
                       agentID = setAgentID();
                       hostMote.setGuestAgentID(agentID);
                   }

               } else {

                   randomWalkOneStep();
                   initializeTimer();
               }

            }

            // Since it is now phase2, the agent simply needs to alert the mote
            // that it will be settling here, then clone to all neighbors that
            // are not red and do not already have an agent present.
            hostMote.setAgentSettledHere();

            while(hostMote.getMoteState() == MoteState.BLUE) {
                Thread.sleep(100);

                //wait for mote to turn yellow
            }

            if(hostMote.getMoteState() == MoteState.YELLOW){
                cloneToNeighbors();
            }

        }catch(InterruptedException e){
            System.err.println(e.getMessage());
        }

        return null;
    }

    /**
     * Finds first neighboring mote that's not red and doesn't already have
     * an agent present. Lets the current mote know that this agent is leaving,
     * then alerts the new mote that it has arrived.
     */
    private void randomWalkOneStep(){
        Mote hostMoteNeighbor = hostMote.getRandomNeighborWithoutAgent();

        if(hostMoteNeighbor != null){
            hostMote.setMobileAgentPresent(false);
            hostMote = hostMoteNeighbor;
            hostMote.setMobileAgentPresent(true);
        }
    }


    /**
     * For each neighbor that doesn't already have an agent nor is red,
     * creates a new agent (in phaseTwo), and begins the thread.
     */
    private void cloneToNeighbors(){
        for(Mote mote : hostMote.getNeighbors()){
            if(!mote.isMobileAgentPresent() &&
                mote.getMoteState() != MoteState.RED){
                Agent agent = new Agent(mote, false);
                Thread thread = new Thread(agent);
                thread.setDaemon(true);
                thread.start();
            }
        }
    }

    /**
     * Method to create and set an Agent's ID.
     * The first agent created will have its ID set to "-1-1-1". Once it
     * settles on a mote, its ID will change (and all cloned agents will
     * follow the following format) to a concatenation of the settled
     * motes x + y + z coordinates.
     * Example: if Mote's MoteLocation is (0, 1, 1), this agent's ID
     * will be "011".
     * @return agent ID as a String
     */
    private String setAgentID(){
        String x = Integer.toString(hostMote.getLocation().getX());
        String y = Integer.toString(hostMote.getLocation().getY());
        String z = Integer.toString(hostMote.getLocation().getZ());

        return isPhaseOne ? "-1-1-1" : x + y + z;
    }

    /**
     * Initializes a MoteTimer that controls the speed (really the
     * frequency of transitions) of the agent's random walk.
     */
    private void initializeTimer(){
        timer = new MoteTimer(AGENT_WALK_SPEED);
        Thread thread = new Thread(timer);
        thread.setDaemon(true);
        thread.start();
    }
}
