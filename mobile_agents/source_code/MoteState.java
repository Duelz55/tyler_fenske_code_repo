package MobileAgents;

/**
 * Provides a set of 3 predefined "states" for a Mote:
 * RED indicates a Mote on fire; YELLOW indicates a Mote with
 * a neighbor on fire; BLUE is the default for other conditions.
 * @author Tyler Fenske
 * @author Warren D. Craft
 */
public enum MoteState{ BLUE, YELLOW, RED }
